//Implementacja dla LAB9

// Kwiatki jako instancje delikatnie się ruszają a la na wietrze - działa

// SHADOWS zaimplementowane, dodane w shaderze phong - nie działa - Po ponad 8 godzinach spędzonych nad tym w tym tygodniu poddałem się i oddaję to zadanie z niewyświetlającymi się cieniami.

// loopback obiekt na środku sceny - działa

// minimapa po prawej u góry - działa 



#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "animation.hpp"
#include "definitions.hpp"
#include "shader_stuff.hpp"
#include "texture_loader.hpp"
#include "obj_loader.hpp"
#include "object.hpp"
#include "light.hpp"
#include "menu.hpp"
#include "cfont.hpp"
#include "shadow.hpp"


int window_height;
int window_widht;

glm::mat4 matProj;
glm::mat4 matMinimap;
glm::mat4 matView;
glm::mat4 matMove;
glm::mat4 matProjView;


glm::vec3 Camera_Position;

glm::vec3 background_color;

Light key_light(-10.0f, 5.1f, 0.0f);
GLuint prog_no_light;
GLuint prog_no_light_instances;
GLuint prog_phong;
GLuint prog_gouraud;
GLuint prog_distance;
GLuint prog_fog;
GLuint prog_reflect;
GLuint prog_skybox;
GLuint prog_shadow;


GLuint FrameBufferID;
GLuint DepthBufferID;
GLuint TextureBuffer;

GLuint mFrameBufferID;
GLuint mDepthBufferID;
GLuint mTextureBuffer;
Object objects[OBJECT_COUNT];
Object_instanced flowers;
Object light_test_box;
Object susan;
Object square;
Object minimap;
Skybox skybox;
Shadows shadows;

TextMessage hud;
TextMessage fps_hud;






// ---------------------------------------
void drawToShadowMap()
{
    glUseProgram(prog_shadow);

    glUniformMatrix4fv( glGetUniformLocation( prog_shadow, "lightProj" ), 1, GL_FALSE, glm::value_ptr(shadows.lightProj) );
    glUniformMatrix4fv( glGetUniformLocation( prog_shadow, "lightView" ), 1, GL_FALSE, glm::value_ptr(shadows.lightView) );

    for (int i = 0; i < OBJECT_COUNT; i++)
    {
        objects[i].setView(matProj * matView);
        objects[i].draw();
    }
    flowers.setView(matProj * matView);
    flowers.draw();
    susan.setView(matProj * matView);
    susan.draw();

    glUseProgram(0);
}

// ---------------------------------------
void drawOnScreen()
{
    // --------------- Ludzie i podłoga --------------------
    for (int i = 0; i < OBJECT_COUNT; i++)
    {
        // if(i!=0)
        // {
            // objects[i].rotate(0.0f, 0.0f, 0.01f);
        // }


        // Obliczanie macierzy rzutowania
        objects[i].setView(matProj * matView);


            //LIGHT mode switch
            switch (light_mode)
            {
                case NO_LIGHT:
                    objects[i].idProgram = prog_no_light;
                    break;
                case PHONG:
                    objects[i].idProgram = prog_phong;
                    break;
                case GOURAUD:
                    objects[i].idProgram = prog_gouraud;
                    break;
                case LIGHT_DISTANCE:
                    objects[i].idProgram = prog_distance;
                    break;
                case LIGHT_FOG:
                    objects[i].idProgram = prog_fog;
                    break;
            }

        // Wlaczenie VAO i programu
        glUseProgram( objects[i].idProgram );

        //LIGHT
        key_light.sendUniform(objects[i].idProgram, Camera_Position);

        objects[i].sendUniform(background_color, shadows.DepthMap_Texture);


        objects[i].draw();
    }

    //------------- Kwiatki ------------------------

    // Obliczanie macierzy rzutowania
    flowers.setView(matProj * matView);

    // Wlaczenie VAO i programu
    glUseProgram( flowers.idProgram );

    //LIGHT Uniform
    key_light.sendUniform(flowers.idProgram, Camera_Position);


    flowers.sendUniform(background_color, shadows.DepthMap_Texture);

    flowers.draw();

    //------------- loopback ------------------------

    // Obliczanie macierzy rzutowania
    square.setView(matProj * matView);

    // Wlaczenie VAO i programu
    glUseProgram( square.idProgram );

    //LIGHT Uniform
    key_light.sendUniform(square.idProgram, Camera_Position);

    square.TextureID = TextureBuffer;

    square.sendUniform(background_color, shadows.DepthMap_Texture);

    square.draw();

    //------------- minimap ------------------------

    // Obliczanie macierzy rzutowania
    minimap.setView(glm::mat4(1.0f));

    // Wlaczenie VAO i programu
    glUseProgram( minimap.idProgram );

    //LIGHT Uniform
    key_light.sendUniform(minimap.idProgram, Camera_Position);

    minimap.TextureID = mTextureBuffer;

    minimap.sendUniform(background_color, shadows.DepthMap_Texture);

    minimap.draw();
    //------------- Susan ------------------------

        // Obliczanie macierzy rzutowania
        susan.setView(matProj * matView);

        // Wlaczenie VAO i programu
        glUseProgram( susan.idProgram );


        //LIGHT Uniform
        key_light.sendUniform(susan.idProgram, Camera_Position);

        susan.sendUniform(background_color, shadows.DepthMap_Texture);

        //REFLECTION/REFRACTION
            // Aktywacja tekstury CUBE_MAP
        glActiveTexture(GL_TEXTURE1);
        glBindTexture( GL_TEXTURE_CUBE_MAP, skybox.SkyBox_Texture );
        glUniform1i(glGetUniformLocation(susan.idProgram, "tex1"), 1);

        susan.draw();


	// Wylaczanie
	glUseProgram( 0 );
}

// ---------------------------------------
// ---------------------------------------
void minimapDraw()
{
    // --------------- Ludzie i podłoga --------------------
    for (int i = 0; i < OBJECT_COUNT; i++)
    {
        // if(i!=0)
        // {
            // objects[i].rotate(0.0f, 0.0f, 0.01f);
        // }


        // Obliczanie macierzy rzutowania
        objects[i].setView(matMinimap * matMove);


            //LIGHT mode switch
            switch (light_mode)
            {
                case NO_LIGHT:
                    objects[i].idProgram = prog_no_light;
                    break;
                case PHONG:
                    objects[i].idProgram = prog_phong;
                    break;
                case GOURAUD:
                    objects[i].idProgram = prog_gouraud;
                    break;
                case LIGHT_DISTANCE:
                    objects[i].idProgram = prog_distance;
                    break;
                case LIGHT_FOG:
                    objects[i].idProgram = prog_fog;
                    break;
            }

        // Wlaczenie VAO i programu
        glUseProgram( objects[i].idProgram );

        //LIGHT
        key_light.sendUniform(objects[i].idProgram, Camera_Position);

        objects[i].sendUniform(background_color, shadows.DepthMap_Texture);


        objects[i].draw();
    }

    //------------- Kwiatki ------------------------

    // Obliczanie macierzy rzutowania
    flowers.setView(matMinimap * matMove);

    // Wlaczenie VAO i programu
    glUseProgram( flowers.idProgram );

    //LIGHT Uniform
    key_light.sendUniform(flowers.idProgram, Camera_Position);


    flowers.sendUniform(background_color, shadows.DepthMap_Texture);

    flowers.draw();

    //------------- loopback ------------------------

    // Obliczanie macierzy rzutowania
    square.setView(matMinimap * matMove);

    // Wlaczenie VAO i programu
    glUseProgram( square.idProgram );

    //LIGHT Uniform
    key_light.sendUniform(square.idProgram, Camera_Position);

    square.TextureID = TextureBuffer;

    square.sendUniform(background_color, shadows.DepthMap_Texture);

    square.draw();

    //------------- Susan ------------------------

        // Obliczanie macierzy rzutowania
        susan.setView(matMinimap * matMove);

        // Wlaczenie VAO i programu
        glUseProgram( susan.idProgram );


        //LIGHT Uniform
        key_light.sendUniform(susan.idProgram, Camera_Position);

        susan.sendUniform(background_color, shadows.DepthMap_Texture);

        //REFLECTION/REFRACTION
            // Aktywacja tekstury CUBE_MAP
        glActiveTexture(GL_TEXTURE1);
        glBindTexture( GL_TEXTURE_CUBE_MAP, skybox.SkyBox_Texture );
        glUniform1i(glGetUniformLocation(susan.idProgram, "tex1"), 1);

        susan.draw();


	// Wylaczanie
	glUseProgram( 0 );
}

// ---------------------------------------
void DisplayScene()
{

    	// 1. Renderowanie z pozycji swiatla do textury DepthMap
	glViewport(0, 0, shadows.DepthMap_Width, shadows.DepthMap_Height);
	glBindFramebuffer(GL_FRAMEBUFFER, shadows.DepthMap_FrameBuffer);
	glClear(GL_DEPTH_BUFFER_BIT);

		drawToShadowMap();

        //2. render do tekstury -minimap

    glViewport(0, 0, window_widht, window_height);
    glBindFramebuffer(GL_FRAMEBUFFER, mFrameBufferID);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        minimapDraw();

        //2. render do tekstury - loopback

    glViewport(0, 0, window_widht, window_height);
    glBindFramebuffer(GL_FRAMEBUFFER, FrameBufferID);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        drawOnScreen();

	// 3. Renderowanie z pozycji kamery na ekran
	glViewport(0, 0, window_widht, window_height);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );


    // Macierz widoku
    matView = glm::mat4x4( 1.0 );
    matView = glm::rotate( matView, _scene_rotate_x, glm::vec3( 1.0f, 0.0f, 0.0f ) );
    matView = glm::rotate( matView, _scene_rotate_y, glm::vec3( 0.0f, 1.0f, 0.0f ) );


    //------------- Skybox ------------------------

    // glUseProgram(skybox.SkyBox_Program);

        // // Przeskalowanie boxa i przeslanie macierzy rzutowania
        // matProjView = matProj * matView * glm::scale(glm::mat4(1), glm::vec3(40.0, 40.0, 40.0));
        // glUniformMatrix4fv( glGetUniformLocation( skybox.SkyBox_Program, "Matrix_proj_view" ), 1, GL_FALSE, glm::value_ptr(matProjView) );
        // glUniform1i(glGetUniformLocation(skybox.SkyBox_Program, "tex0"), 0);

        // // Aktywacja tekstury CUBE_MAP
        // glActiveTexture(GL_TEXTURE0);
        // glBindTexture( GL_TEXTURE_CUBE_MAP, skybox.SkyBox_Texture );

        // // Rendering boxa
        // glBindVertexArray( skybox.SkyBox_VAO );
            // glDrawElements( GL_TRIANGLES, 12 * 3, GL_UNSIGNED_INT, NULL );
        // glBindVertexArray( skybox.SkyBox_VAO );



    //Move view
    matMove = glm::translate( matMove, glm::vec3( _scene_translate_x, _scene_translate_y, _scene_translate_z ) );
    matView = matView * matMove;

    //CAMERA_POS for LIGHT purpuses
	Camera_Position = ExtractCameraPos(matView);


    drawOnScreen();

    hud.drawHUD();
    fps_hud.drawFPS(fps);


	glutSwapBuffers();

}



// ---------------------------------------
void Initialize()
{


    matMove = glm::mat4x4( 1.0 );
	// _scene_translate_z = -10.0f;
	_scene_translate_y = -2.0f;

	background_color = glm::vec3( 0.2f, 0.2f, 0.2f);
	glClearColor(background_color.r, background_color.g, background_color.b, 1.0f);


    //1. Program bez światła
    prog_no_light = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_no_light, LoadShader(GL_VERTEX_SHADER, "shader/vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_no_light, LoadShader(GL_FRAGMENT_SHADER, "shader/fragment.glsl"));

    LinkAndValidateProgram( prog_no_light );

    //2. Program światło Phong
    prog_phong = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_phong, LoadShader(GL_VERTEX_SHADER, "shader/phong_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_phong, LoadShader(GL_FRAGMENT_SHADER, "shader/phong_fragment.glsl"));

    LinkAndValidateProgram( prog_phong );

    //3. Program światło Gouraud
    prog_gouraud = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_gouraud, LoadShader(GL_VERTEX_SHADER, "shader/gouraud_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_gouraud, LoadShader(GL_FRAGMENT_SHADER, "shader/gouraud_fragment.glsl"));

    LinkAndValidateProgram( prog_gouraud );

    //4. Program światło gouraud zanikające z odległością
    prog_distance = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_distance, LoadShader(GL_VERTEX_SHADER, "shader/distance_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_distance, LoadShader(GL_FRAGMENT_SHADER, "shader/distance_fragment.glsl"));

    LinkAndValidateProgram( prog_distance );

    //5. Mgła
    prog_fog = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_fog, LoadShader(GL_VERTEX_SHADER, "shader/fog_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_fog, LoadShader(GL_FRAGMENT_SHADER, "shader/fog_fragment.glsl"));

    LinkAndValidateProgram( prog_fog );

    //6. Reflective
    prog_reflect = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_reflect, LoadShader(GL_VERTEX_SHADER, "shader/reflect_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_reflect, LoadShader(GL_FRAGMENT_SHADER, "shader/reflect_fragment.glsl"));

    LinkAndValidateProgram( prog_reflect );

    //7. Skybox
    prog_skybox = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_skybox, LoadShader(GL_VERTEX_SHADER, "shader/skybox_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_skybox, LoadShader(GL_FRAGMENT_SHADER, "shader/skybox_fragment.glsl"));

    LinkAndValidateProgram( prog_skybox );

    //8. No light (plane with alpha texture) instances
    prog_no_light_instances = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_no_light_instances, LoadShader(GL_VERTEX_SHADER, "shader/no_light_instances_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_no_light_instances, LoadShader(GL_FRAGMENT_SHADER, "shader/no_light_instances_fragment.glsl"));

    LinkAndValidateProgram( prog_no_light_instances );

    //9. Program cieni
    prog_shadow = glCreateProgram();
    glAttachShader( prog_shadow, LoadShader(GL_VERTEX_SHADER, "shader/shadow_vertex.glsl"));
    glAttachShader( prog_shadow, LoadShader(GL_FRAGMENT_SHADER, "shader/shadow_fragment.glsl"));
    LinkAndValidateProgram( prog_shadow );

    //---------------------------------------------


	glEnable(GL_DEPTH_TEST);
    glEnable( GL_BLEND );
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //---------- Ludzie i podłoga -----------------
    for (int i = 0; i < OBJECT_COUNT; i++)
    {
        switch (light_mode)
        {
            case NO_LIGHT:
                objects[i].idProgram = prog_no_light;
                break;
            case PHONG:
                objects[i].idProgram = prog_phong;
                break;
            case GOURAUD:
                objects[i].idProgram = prog_gouraud;
                break;
            case LIGHT_DISTANCE:
                objects[i].idProgram = prog_distance;
                break;
            case LIGHT_FOG:
                objects[i].idProgram = prog_distance;
                break;
        }


        switch (i)
        {
            case 0:
                objects[i].init("obj/base.obj", "tex/base.bmp");
                break;
            case 1:
                objects[i].init("obj/humans.obj", "tex/humans.bmp");
                break;
            case 2:
                objects[i].init("obj/bride_clothes.obj", "tex/bride_clothes.bmp");
                break;
            case 3:
                objects[i].init("obj/groom_clothes.obj", "tex/groom_clothes.bmp");
                break;
        }


    }

    //---------- Kwiatki -----------------
    flowers.idProgram = prog_no_light_instances;
    flowers.init("obj/flower.obj", "tex/flower.png");
    flowers.setInstances(500);
    // flowers.translate(4.0f, 0.0f, 0.0f);

    //---------- Susan -----------------
    susan.idProgram = prog_reflect;
    susan.init("obj/susan.obj", "tex/susan.png");
    susan.translate(0.0f, 3.0f, 5.0f);
    susan.rotate(0.0f, 3.0f);

    //---------- Skybox -----------------
    skybox.SkyBox_Program = prog_skybox;
    skybox.CreateSkyBox();

    //------------loopback -------------
    square.idProgram = prog_no_light;
    square.init("obj/square.obj", "tex/susan.png");
    // minimap.rotate(0.0f, glm::radians(180.0f), glm::radians(180.0f));
    square.translate(0.0f, 1.0f);

    //------------Minimap -------------
    minimap.idProgram = prog_no_light;
    minimap.init("obj/square.obj", "tex/susan.png");
    minimap.translate(0.7f, 0.8f);
    minimap.scale(0.2f);
    // minimap.rotate(0.0f, glm::radians(180.0f), glm::radians(180.0f));

    //----------- Fonts ----------------

    hud.init("font/xirod.ttf");
    fps_hud.init("font/space_age.ttf");










    // shadows.init();



}

// ---------------------------------------
void Reshape( int width, int height )
{
	glViewport( 0, 0, width, height );

	matProj = glm::ortho(-1.0, 1.0, -1.0, 1.0);
	matProj = glm::perspective(glm::radians(80.0f), width/(float)height, 0.1f, 200.0f);
	matMinimap = glm::ortho(-1.0, 1.0, -1.0, 1.0);
	matMinimap = glm::perspective(glm::radians(80.0f), width/(float)height, 0.1f, 200.0f);
    matMinimap = glm::rotate(matMinimap, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    matMinimap = glm::translate(matMinimap, glm::vec3(-10.0f, -10.0f, 0.0f));

    hud.reshape(width, height);
    fps_hud.reshape(width, height);

    window_widht = width;
    window_height = height;
    // --------------------------------------------------
    // 1. Stworzenie tekstury do ktorej odbedzie sie rendering

    glGenTextures(1, &mTextureBuffer);
    glBindTexture(GL_TEXTURE_2D, mTextureBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, window_widht, window_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // --------------------------------------------------
    // 2. Stworzenie obiektu bufora ramki, ktory zawiera bufor koloru

    glGenFramebuffers(1, &mFrameBufferID);
    glBindFramebuffer(GL_FRAMEBUFFER, mFrameBufferID);

    std::cout << "Color buffer: " << mTextureBuffer << std::endl;

    // Polaczenie tekstury do skladowej koloru bufora ramki
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mTextureBuffer, 0);

    // --------------------------------------------------
    // 3. Stworzenie obiektu render-buffer dla testu glebokosci

    glGenRenderbuffers(1, &mDepthBufferID);
    glBindRenderbuffer(GL_RENDERBUFFER, mDepthBufferID);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, window_widht, window_height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    std::cout << "Depth buffer: " << DepthBufferID << std::endl;
    // Polaczenie bufora glebokosci z aktualnym obiektem bufora ramki
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, DepthBufferID);

    // --------------------------------------------------
    // 4. Sprawdzenie czy pomyslnie zostal utworzony obiekt bufora ramki
    // i powrot do domyslnego bufora ramki
    auto glstatus = glGetError();
    if (glstatus != GL_NO_ERROR) { std::cout << "Error in GL call: " << glstatus << std::endl; } 

    auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if( status != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "Frame buffer status: " << status << std::endl;
        std::cout << "Unsupporetd: " << GL_FRAMEBUFFER_UNSUPPORTED << std::endl;
        printf("window_size: %d, %d", window_widht, window_height);
        printf("Error: Framebuffer is not complete!\n");
        exit(1);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // --------------------------------------------------
    // 1. Stworzenie tekstury do ktorej odbedzie sie rendering

    glGenTextures(1, &TextureBuffer);
    glBindTexture(GL_TEXTURE_2D, TextureBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, window_widht, window_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // --------------------------------------------------
    // 2. Stworzenie obiektu bufora ramki, ktory zawiera bufor koloru

    glGenFramebuffers(1, &FrameBufferID);
    glBindFramebuffer(GL_FRAMEBUFFER, FrameBufferID);

    std::cout << "Color buffer: " << TextureBuffer << std::endl;

    // Polaczenie tekstury do skladowej koloru bufora ramki
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, TextureBuffer, 0);

    // --------------------------------------------------
    // 3. Stworzenie obiektu render-buffer dla testu glebokosci

    glGenRenderbuffers(1, &DepthBufferID);
    glBindRenderbuffer(GL_RENDERBUFFER, DepthBufferID);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, window_widht, window_height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    std::cout << "Depth buffer: " << DepthBufferID << std::endl;
    // Polaczenie bufora glebokosci z aktualnym obiektem bufora ramki
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, DepthBufferID);

    // --------------------------------------------------
    // 4. Sprawdzenie czy pomyslnie zostal utworzony obiekt bufora ramki
    // i powrot do domyslnego bufora ramki
    glstatus = glGetError();
    if (glstatus != GL_NO_ERROR) { std::cout << "Error in GL call: " << glstatus << std::endl; } 

    status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if( status != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "Frame buffer status: " << status << std::endl;
        std::cout << "Unsupporetd: " << GL_FRAMEBUFFER_UNSUPPORTED << std::endl;
        printf("window_size: %d, %d", window_widht, window_height);
        printf("Error: Framebuffer is not complete!\n");
        exit(1);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // 1. Stworzenie tekstury
    glGenTextures(1, &shadows.DepthMap_Texture);
    glBindTexture(GL_TEXTURE_2D, shadows.DepthMap_Texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadows.DepthMap_Width, shadows.DepthMap_Height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

    // 2. Stworzenie frame buffera
    glGenFramebuffers(1, &shadows.DepthMap_FrameBuffer);

    // 3. Polaczenie tekstury i framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, shadows.DepthMap_FrameBuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadows.DepthMap_Texture, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if( status != GL_FRAMEBUFFER_COMPLETE) {
        std::cout << "SHADOWS Frame buffer status: " << status << std::endl;
        std::cout << "Unsupporetd: " << GL_FRAMEBUFFER_UNSUPPORTED << std::endl;
        printf("window_size: %d, %d", window_widht, window_height);
        printf("Error: Framebuffer is not complete!\n");
        exit(1);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

}


// ---------------------------------------------------
int main( int argc, char *argv[] )
{
	// GLUT
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH );
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );
	glutInitWindowSize( 500, 500 );
	glutCreateWindow( "Programownie grafiki w OpenGL" );

	// -----------------------------
	// NOWE: Handlery
	// -----------------------------

	glutDisplayFunc( DisplayScene );
	glutReshapeFunc( Reshape );
	glutMouseFunc( MouseButton );
	glutMotionFunc( MouseMotion );
	glutMouseWheelFunc( MouseWheel );
	glutKeyboardFunc( KeyDown );
	glutKeyboardUpFunc( KeyUp );
	glutSpecialFunc( SpecialKeys );


	// GLEW
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if( GLEW_OK != err )
	{
		printf("GLEW Error\n");
		exit(1);
	}

	// OpenGL
	if( !GLEW_VERSION_3_2 )
	{
		printf("Brak OpenGL 3.2!\n");
		exit(1);
	}

    //Menu

    int subMenuLight = glutCreateMenu( menu_stuff );
    glutAddMenuEntry( "Ambient", MENU_NO_LIGHT );
    glutAddMenuEntry( "Phong", MENU_PHONG );
    glutAddMenuEntry( "Gouraud", MENU_GOURAUD );
    glutAddMenuEntry( "Ambient", MENU_NO_LIGHT );
    glutAddMenuEntry( "Distance", MENU_LIGHT_DISTANCE );
    glutAddMenuEntry( "Fog", MENU_LIGHT_FOG );

    // Kolejne podmenu
    int SubMenuDummy = glutCreateMenu( menu_stuff );
    glutAddMenuEntry( "Dummy 1", -1 );
    glutAddMenuEntry( "Dummy 2", -1 );

    // Na koniec utworzenie glownego menu
    glutCreateMenu( menu_stuff );
    glutAddSubMenu( "Swiatlo ogolne", subMenuLight );
    glutAddSubMenu( "Dummy", SubMenuDummy );

    glutAttachMenu( GLUT_RIGHT_BUTTON );


	Initialize();

       // glutTimerFunc (100 , Animation , 0);
	glutTimerFunc ((int)(1000/60) , Animation ,0);
	glutMainLoop (); 



	// Cleaning
	glDeleteProgram(prog_no_light);
	glDeleteProgram(prog_phong);
	glDeleteProgram(prog_gouraud);
	glDeleteProgram(prog_distance);
	glDeleteProgram(prog_fog);
	glDeleteProgram(prog_skybox);
	glDeleteProgram(prog_reflect);
	glDeleteProgram(prog_no_light_instances);
    glDeleteProgram(prog_shadow);

	return 0;
}
