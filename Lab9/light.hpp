#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class Light {
    public:
        glm::vec3 Light_Ambient;
        glm::vec3 Light_Diffuse;
        glm::vec3 Light_Specular;
        glm::vec3 Light_Position;
        glm::vec3 Light_Direction = glm::normalize(glm::vec3(0.2, -0.8f, 1.1f));
        float Light_Attenuation;


        Light(float x = 0.0f, float y = 0.0f, float z = 0.0f, float attenuation = 0.01f, float a_x = 0.2f, float a_y = 0.2f, float a_z = 0.2f, float d_x = 1.0f, float d_y = 1.0f, float d_z = 1.0f, float s_x = 0.9f, float s_y = 0.9f, float s_z = 0.9f)
        {
            Light_Ambient = glm::vec3(a_x, a_y, a_z);
            Light_Diffuse = glm::vec3(d_x, d_y, d_z);
            Light_Specular = glm::vec3(s_x, s_y, s_z);
            Light_Position = glm::vec3(x, y, z);
            Light_Attenuation = attenuation;
        }

        void sendUniform(GLuint &program, glm::vec3 &Camera_Position)
        {
            glUniform3fv( glGetUniformLocation( program, "Light_Ambient" ), 1, &Light_Ambient[0] );
            glUniform3fv( glGetUniformLocation( program, "Light_Diffuse" ), 1, &Light_Diffuse[0] );
            glUniform3fv( glGetUniformLocation( program, "Light_Specular" ), 1, &Light_Specular[0] );
            glUniform3fv( glGetUniformLocation( program, "Light_Position" ), 1, &Light_Position[0] );
            glUniform1f( glGetUniformLocation( program, "Light_Attenuation" ), Light_Attenuation);
            glUniform3fv( glGetUniformLocation( program, "Camera_Position" ), 1, &Camera_Position[0] );
            glUniform3fv( glGetUniformLocation( program, "Light_Direction" ), 1, glm::value_ptr(Light_Direction) );
        }

};
