#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>



// ------------------------------------
// Shadow mapping
// ------------------------------------

class Shadows
{
    public:

        // ---------------------------------------
        // Zalozenia: model oswietlenia kierunkowego
        glm::vec3 Light_Direction = glm::normalize(glm::vec3(0.0f, -100.0f, 0.0f));
        glm::vec3 Light_Position = glm::vec3(0.0f, 100.0f, 0.0f);


        // ---------------------------------------
        // Identyfikatory obiektów
        GLuint DepthMap_Program;		// program
        GLuint DepthMap_FrameBuffer;	// frame buffer
        GLuint DepthMap_Texture;		// texture


        // ---------------------------------------
        // Parametry Shadow Mapy
        const unsigned int DepthMap_Width = 1024, DepthMap_Height = 1024;

        // ---------------------------------------
        // Macierze rzutowania
        glm::mat4 lightProj = glm::ortho(-30.0f, 30.0f, -30.0f, 30.0f, 1.0f, 60.5f);
        glm::mat4 lightView = glm::lookAt(Light_Position,
                                          Light_Position+Light_Direction,
                                          glm::vec3( 0.0f, 1.0f,  0.0f));



        // ---------------------------------------
        // INICJALIZACJA
        // ---------------------------------------
        void init()
        {


            // 1. Stworzenie tekstury
            glGenTextures(1, &DepthMap_Texture);
            glBindTexture(GL_TEXTURE_2D, DepthMap_Texture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, DepthMap_Width, DepthMap_Height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
            float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
            glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

            // 2. Stworzenie frame buffera
            glGenFramebuffers(1, &DepthMap_FrameBuffer);

            // 3. Polaczenie tekstury i framebuffer
            glBindFramebuffer(GL_FRAMEBUFFER, DepthMap_FrameBuffer);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, DepthMap_Texture, 0);
            glDrawBuffer(GL_NONE);
            glReadBuffer(GL_NONE);
            glBindFramebuffer(GL_FRAMEBUFFER, 0);

        }

};
