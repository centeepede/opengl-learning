#version 330

in vec3 inoutNormal;
in vec2 inoutUV;
out vec4 outColor;

uniform vec3 color_base;
uniform sampler2D tex0;


void main()
{
    // float l = inoutNormal.r+inoutNormal.g+inoutNormal.b;

    // vec3 myColor = vec3(l, l, l) * color_base;

    vec4 texColor = texture( tex0, inoutUV ) ;// + vec4(inoutColor, 1.0) ;


    if(texColor.a < 0.2f)
    {
        discard;
    }
    // outColor = vec4(myColor, 1.0);
    outColor = texColor;
}
