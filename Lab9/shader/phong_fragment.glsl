#version 150 core

in vec3 ourPosition;
in vec3 inoutNormal;
in vec2 inoutUV;
in vec4 ourPosLight;


out vec4 outColor;

uniform sampler2D tex0;
uniform sampler2D shadowMap;


// Parametry oswietlenia
uniform vec3 Light_Ambient;
uniform vec3 Light_Diffuse;
uniform vec3 Light_Specular;
uniform vec3 Light_Position;
uniform vec3 Light_Direction;
uniform vec3 Camera_Position;



// ---------------------------------------------------------------------------
// Zwraca [0-1], gdzie 1 oznacza ze fragment jest calkowicie w cieniu
// ---------------------------------------------------------------------------
float Calculate_Shadow(vec4 fragPosLight, vec3 LightDirection)
{

	// Debug: sprawdzenie zasiegu cienia
	return 0.0f;

	// Korekcja perspektywiczna (dla oswietlenia kierunkowego niepotrzebna)
    vec3 projCoords = fragPosLight.xyz / fragPosLight.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5f + 0.5f;

    // Brak cienia poza zasiegiem "kamery cienia" patrzac w dal
    //if(projCoords.z > 1.0)
    //    return 0.0f;


    // pobranie z tekstury shadowMap odleglosci od swiatla fragmentu oswietlonego
    float closestDepth = texture(shadowMap, projCoords.xy).r;

    // obliczenie aktualnej odleglosci od swiatla
    float currentDepth = projCoords.z;

    // Metoda 1: proste sprawdzenie czy fragment jest w cieniu po odleglosci
	return (currentDepth > closestDepth) ? 1.0f : 0.0f;


	// -------------------------------------------
	// Efekty uboczne: Shadow Acne i Peter Panning
	// -------------------------------------------

	float bias = 0.0f;
    bias = max(0.004 * (1.0f - dot(inoutNormal, Light_Direction)), 0.001f);

	// Metoda 2: usuniecie shadow acne
	return (currentDepth - bias > closestDepth) ? 1.0f : 0.0f;



	// Metoda 3: Smooth filtering
	float shadow = 0.0f;
	vec2 texelSize = 1.0f / textureSize(shadowMap, 0);
	for(float x = -1.0f; x <= 1.0f; x+=1.0f)
	{
		for(float y = -1.0f; y <= 1.0f; y+=1.0f)
		{
			float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
			shadow += currentDepth - bias > pcfDepth ? 1.0f : 0.0f;
		}
	}
	shadow /= 9.0f;


	return shadow;
}

// ---------------------------------------------------------------------------
vec3 Calculate_PointLight(vec3 fragPos, vec3 fragNormal)
{
	// ---------------------------
	// Ambient
	vec3 ambientPart = Light_Ambient;


	// ---------------------------
	// Diffuse

	// Obliczenie wektora (swiatlo - wierzcholek)
	// czyli kierunku padania swiatla na wierzcholek
	vec3 lightDirection = normalize(Light_Position - vec3(fragPos));

	// obliczenie kata pomiedzy wektorem lightDir oraz wektorem normalnym
	// wartosc kata okresla pod jakim katem padaja promienie
	float lightCoeff = max(dot(fragNormal, lightDirection), 0.0f);

	vec3 diffusePart = lightCoeff * Light_Diffuse;


	// ------------------
	// Specular
	vec3 viewDir = normalize(Camera_Position - vec3(fragPos));
	vec3  reflectDir = reflect(-lightDirection, fragNormal);
	// obliczanie wspolczynnika specular z parametrem shininess
	float specularCoeff = pow(max(dot(viewDir, reflectDir), 0.0), 128.0f);
	vec3  specularPart = specularCoeff * Light_Specular;

	// -----------------
	// Ostateczny
	return (ambientPart + diffusePart + specularPart);
}


void main()
{

	// ustawienie domyslnego koloru fragmentu
	// mozna tutaj uwzglednic tekstury i inne parametry
	// vec3 objectColor = vec3(0.2, 0.8, 0.2);
    vec4 objectColor = texture( tex0, inoutUV ) ;// + vec4(inoutColor, 1.0) ;


	// Cienie
	float shadowPart = Calculate_Shadow(ourPosLight, Light_Direction);

	// Zastosowanie oswietlenia do fragmentu
    outColor = vec4(Calculate_PointLight(ourPosition, inoutNormal), 1.0f) * objectColor * (1-shadowPart);
    // outColor = vec4(shadowPart, shadowPart, shadowPart, 1.0f);


}
