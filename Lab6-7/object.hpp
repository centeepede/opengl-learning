#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_stuff.hpp"
#include "obj_loader.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "texture_loader.hpp"
class Object {

    private:
        glm::mat4 _matObject;
        float _color[3];



    public:
        std::vector<glm::vec3> OBJ_vertices;
        std::vector<glm::vec2> OBJ_uvs;
        std::vector<glm::vec3> OBJ_normals;
        GLuint idProgram;	// programu
        GLuint vArray;
        // GLuint vBuffer_coord;	// bufora na wspolrzedne
        // GLuint vBuffer_normal; // bufora na kolory
        // GLuint vBuffer_uv; // bufora na kolory
        GLuint TextureID;
        GLuint Size;


        glm::mat4 getView()
        {

            return(_matObject);
        }

        void rotate(float x = 0.0f, float y = 0.0f, float z = 0.0f)
        {
            _matObject = glm::rotate( _matObject, y, glm::vec3( 0.0f, 1.0f, 0.0f ) );
            _matObject = glm::rotate( _matObject, x, glm::vec3( 1.0f, 0.0f, 0.0f ) );
            _matObject = glm::rotate( _matObject, z, glm::vec3( 0.0f, 0.0f, 1.0f ) );
        }

        void translate(float x = 0.0f, float y = 0.0f, float z = 0.0f)
        {
            _matObject = glm::translate( _matObject, glm::vec3( x, y, z) );
        }

        void scale(float s)
        {
            _matObject = glm::scale( _matObject, glm::vec3(s, s, s) );
        }

        void scale(float x, float y, float z)
        {
            _matObject = glm::scale( _matObject, glm::vec3( x, y, z) );
        }

        void draw()
        {
            glBindVertexArray( vArray );
            glDrawArrays( GL_TRIANGLES, 0, Size );
            glBindVertexArray( 0 );
        }

        void init(const char * fileName, const char * texFName, float r = 0.5f, float g = 0.5f, float b = 0.5f)
        {
            _matObject = glm::mat4x4( 1.0 );

            CreateTexture(texFName);

            if (!loadOBJ(fileName, OBJ_vertices, OBJ_uvs, OBJ_normals))
            {
                printf("ERROR: Not loaded %s!\n", fileName);
                exit(1);
            }
            _color[0] = r;
            _color[1] = g;
            _color[2] = b;

            Size = OBJ_vertices.size();

            // Vertex arrays
            glGenVertexArrays( 1, &this->vArray );
            glBindVertexArray( this->vArray );

            // Wspolrzedne wierzchokow
            GLuint vBuffer_coord;
            glGenBuffers( 1, &vBuffer_coord );
            glBindBuffer( GL_ARRAY_BUFFER, vBuffer_coord );
            glBufferData( GL_ARRAY_BUFFER, OBJ_vertices.size() * sizeof(glm::vec3), &OBJ_vertices[0], GL_STATIC_DRAW );

            glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
            glEnableVertexAttribArray( 0 );

            // Wektory normalne
            GLuint vBuffer_normal;
            glGenBuffers( 1, &vBuffer_normal );
            glBindBuffer( GL_ARRAY_BUFFER, vBuffer_normal );
            glBufferData( GL_ARRAY_BUFFER, OBJ_normals.size() * sizeof(glm::vec3), &OBJ_normals[0], GL_STATIC_DRAW );

            glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, NULL );
            glEnableVertexAttribArray( 1 );

            // Wspolrzedne UV
            GLuint vBuffer_uv;
            glGenBuffers( 1, &vBuffer_uv );
            glBindBuffer( GL_ARRAY_BUFFER, vBuffer_uv );
            glBufferData( GL_ARRAY_BUFFER, OBJ_uvs.size() * sizeof(glm::vec2), &OBJ_uvs[0], GL_STATIC_DRAW );
            glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, 0, NULL );
            glEnableVertexAttribArray( 2 );

            glBindVertexArray( 0 );

        }

        void CreateTexture(const char * texFName)
        {
            int tex_width;
            int tex_height;
            int comp;
            auto rgb_mode = GL_RGBA;
            unsigned char* tex_data;

            if (strstr(texFName, ".bmp"))
            {
                loadBMP_custom(texFName, tex_width, tex_height, &tex_data);
                rgb_mode = GL_BGR;
            } else
            {
                tex_data = stbi_load(texFName, &tex_width, &tex_height, &comp, STBI_rgb_alpha);
                if (comp == 3)
                {
                    rgb_mode = GL_RGB;
                } 
                else if (comp == 4)
                {
                    rgb_mode = GL_RGBA;
                    // printf("found flower %s\n", texFName);
                } 
            }

            if(tex_data == NULL)
            {
                printf("ERROR: Texture\"%s\" not loaded!", texFName);
            }

            glGenTextures(1, &TextureID);
            glBindTexture(GL_TEXTURE_2D, TextureID);

            // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex_width, tex_height, 0, rgb_mode, GL_UNSIGNED_BYTE, tex_data);

            stbi_image_free(tex_data);

            // -------------------------------------------------------
            // 4. Ustawianie parametrow tekstury
            // -------------------------------------------------------

            // (a) Zachowanie wyjsca poza wspolrzedne UV (0-1) Wraping

            // GL_REPEAT, GL_MIRRORED_REPEAT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_BORDER
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

            // Ustalenie koloru ramki w przypadku wybrania GL_CLAMP_TO_BORDER
            //float color[] = { 1.0f, 0.0f, 0.0f, 1.0f };
            //glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);

            // (b) Zachowanie tekstury w przypadku powiekszenia

            // GL_LINEAR, GL_NEAREST
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            // (c) Zachowanie tekstury w przypadku pomniejszenia

            // GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_NEAREST,
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);


            // -------------------------------------------------------
            // 5. Gdy wybrano korzystanie z MIPMAP
            // -------------------------------------------------------

            // Generowanie mipmap automatycznie
            glGenerateMipmap(GL_TEXTURE_2D);

            // Podstawowy level mipmap (default 0)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
        }


        float r()
        {
            return _color[0];
        }

        float g()
        {
            return _color[1];
        }

        float b()
        {
            return _color[2];
        }
};
