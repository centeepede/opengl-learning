#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "definitions.hpp"
#include "shader_stuff.hpp"
#include "obj_loader.hpp"
#include "object.hpp"
#include "light.hpp"
#include "animation.hpp"

#include "texture_loader.hpp"

#define OBJECT_COUNT 4
#define FLOWER_COUNT 5


glm::mat4 matProj;
glm::mat4 matView;
glm::mat4 matMove;
glm::mat4 matProjView;


glm::vec3 Camera_Position;

glm::vec3 background_color;

Light key_light(0.0f, 1.1f, 0.0f);
GLuint prog_no_light;
GLuint prog_phong;
GLuint prog_gouraud;
GLuint prog_distance;
GLuint prog_fog;

Object objects[OBJECT_COUNT];
Object flowers[FLOWER_COUNT];
Object light_test_box;


// ---------------------------------------
void DisplayScene()
{

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // Macierz widoku
    matView = glm::mat4x4( 1.0 );
    matView = glm::rotate( matView, _scene_rotate_x, glm::vec3( 1.0f, 0.0f, 0.0f ) );
    matView = glm::rotate( matView, _scene_rotate_y, glm::vec3( 0.0f, 1.0f, 0.0f ) );
    matMove = glm::translate( matMove, glm::vec3( _scene_translate_x, _scene_translate_y, _scene_translate_z ) );
    matView = matView * matMove;

    //CAMERA_POS for LIGHT purpuses
	Camera_Position = ExtractCameraPos(matView);

    // --------------- Ludzie i podłoga --------------------
    for (int i = 0; i < OBJECT_COUNT; i++)
    {
        if(i!=0)
        {
            objects[i].rotate(0.0f, 0.0f, 0.01f);
        }


        // Obliczanie macierzy rzutowania
        matProjView = matProj * matView * objects[i].getView();


            //LIGHT mode switch
            switch (light_mode)
            {
                case NO_LIGHT:
                    objects[i].idProgram = prog_no_light;
                    break;
                case PHONG:
                    objects[i].idProgram = prog_phong;
                    break;
                case GOURAUD:
                    objects[i].idProgram = prog_gouraud;
                    break;
                case LIGHT_DISTANCE:
                    objects[i].idProgram = prog_distance;
                    break;
                case LIGHT_FOG:
                    objects[i].idProgram = prog_fog;
                    break;
            }

        // Wlaczenie VAO i programu
        glUseProgram( objects[i].idProgram );

		// Przekazanie macierzy rzutowania
		glUniformMatrix4fv( glGetUniformLocation(objects[i].idProgram, "matProjView"), 1, GL_FALSE,  glm::value_ptr(matProjView));

        glUniform3fv(glGetUniformLocation(objects[i].idProgram, "background_color"),1,  &background_color[0] );


        //LIGHT
        key_light.sendUniform(objects[i].idProgram, Camera_Position);


		// TEXTURE
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, objects[i].TextureID);
		glUniform1i(glGetUniformLocation(objects[i].idProgram, "tex0"), 0);

        objects[i].draw();
    }

    //------------- Kwiatki ------------------------

    for (int i = 0; i < FLOWER_COUNT; i++)
    {
        // Obliczanie macierzy rzutowania
        matProjView = matProj * matView * flowers[i].getView();

        // Wlaczenie VAO i programu
        glUseProgram( flowers[i].idProgram );

		// Przekazanie macierzy rzutowania
		glUniformMatrix4fv( glGetUniformLocation(flowers[i].idProgram, "matProjView"), 1, GL_FALSE,  glm::value_ptr(matProjView));
        glUniform3fv(glGetUniformLocation(flowers[i].idProgram, "background_color"),1,  &background_color[0] );
        //LIGHT Uniform
        key_light.sendUniform(flowers[i].idProgram, Camera_Position);


		// TEXTURE
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, flowers[i].TextureID);
		glUniform1i(glGetUniformLocation(flowers[i].idProgram, "tex0"), 0);

        flowers[i].draw();
    }

    // ------------------ light distance test box ------------------------
    // Obliczanie macierzy rzutowania
    matProjView = matProj * matView * light_test_box.getView();

    // Wlaczenie VAO i programu
    glUseProgram( light_test_box.idProgram );

    // Przekazanie macierzy rzutowania
    glUniformMatrix4fv( glGetUniformLocation(light_test_box.idProgram, "matProjView"), 1, GL_FALSE,  glm::value_ptr(matProjView));
    glUniform3fv(glGetUniformLocation(light_test_box.idProgram, "background_color"),1,  &background_color[0] );
    //LIGHT Uniform
    key_light.sendUniform(light_test_box.idProgram, Camera_Position);


    // TEXTURE
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, light_test_box.TextureID);
    glUniform1i(glGetUniformLocation(light_test_box.idProgram, "tex0"), 0);

    light_test_box.draw();


	// Wylaczanie
	glUseProgram( 0 );


	glutSwapBuffers();

}



// ---------------------------------------
void Initialize()
{
    matMove = glm::mat4x4( 1.0 );
	// _scene_translate_z = -10.0f;
	_scene_translate_y = -2.0f;

	background_color = glm::vec3( 0.2f, 0.2f, 0.2f);
	glClearColor(background_color.r, background_color.g, background_color.b, 1.0f);


    //1. Program bez światła
    prog_no_light = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_no_light, LoadShader(GL_VERTEX_SHADER, "shader/vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_no_light, LoadShader(GL_FRAGMENT_SHADER, "shader/fragment.glsl"));

    LinkAndValidateProgram( prog_no_light );

    //2. Program światło Phong
    prog_phong = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_phong, LoadShader(GL_VERTEX_SHADER, "shader/phong_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_phong, LoadShader(GL_FRAGMENT_SHADER, "shader/phong_fragment.glsl"));

    LinkAndValidateProgram( prog_phong );

    //3. Program światło Gouraud
    prog_gouraud = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_gouraud, LoadShader(GL_VERTEX_SHADER, "shader/gouraud_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_gouraud, LoadShader(GL_FRAGMENT_SHADER, "shader/gouraud_fragment.glsl"));

    LinkAndValidateProgram( prog_gouraud );

    //4. Program światło gouraud zanikające z odległością
    prog_distance = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_distance, LoadShader(GL_VERTEX_SHADER, "shader/distance_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_distance, LoadShader(GL_FRAGMENT_SHADER, "shader/distance_fragment.glsl"));

    LinkAndValidateProgram( prog_distance );

    //5. Mgła
    prog_fog = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_fog, LoadShader(GL_VERTEX_SHADER, "shader/fog_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_fog, LoadShader(GL_FRAGMENT_SHADER, "shader/fog_fragment.glsl"));

    LinkAndValidateProgram( prog_fog );


	glEnable(GL_DEPTH_TEST);
    glEnable( GL_BLEND );
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //---------- Ludzie i podłoga -----------------
    for (int i = 0; i < OBJECT_COUNT; i++)
    {
        switch (light_mode)
        {
            case NO_LIGHT:
                objects[i].idProgram = prog_no_light;
                break;
            case PHONG:
                objects[i].idProgram = prog_phong;
                break;
            case GOURAUD:
                objects[i].idProgram = prog_gouraud;
                break;
            case LIGHT_DISTANCE:
                objects[i].idProgram = prog_distance;
                break;
            case LIGHT_FOG:
                objects[i].idProgram = prog_distance;
                break;
        }


        switch (i)
        {
            case 0:
                objects[i].init("obj/base.obj", "tex/base.bmp");
                break;
            case 1:
                objects[i].init("obj/humans.obj", "tex/humans.bmp");
                break;
            case 2:
                objects[i].init("obj/bride_clothes.obj", "tex/bride_clothes.bmp");
                break;
            case 3:
                objects[i].init("obj/groom_clothes.obj", "tex/groom_clothes.bmp");
                break;
        }


    }

    //---------- Kwiatki -----------------
    for (int i = 0; i < FLOWER_COUNT; i++)
    {

        flowers[i].idProgram = prog_no_light;
        flowers[i].init("obj/flower.obj", "tex/flower.png");
        flowers[i].translate((float)(2*(sin(i)+4)), 0.0f, (float)(2*cos(i)));
    }

    light_test_box.idProgram = prog_distance;
    light_test_box.init("obj/light_test_box.obj", "tex/light_test_box.png");
    light_test_box.translate(0.0f, 0.0f, 2.0f);
    light_test_box.scale(0.5f);



}

// ---------------------------------------
void Reshape( int width, int height )
{
	glViewport( 0, 0, width, height );

	matProj = glm::ortho(-1.0, 1.0, -1.0, 1.0);
	matProj = glm::perspective(glm::radians(80.0f), width/(float)height, 0.1f, 200.0f);

}


// ---------------------------------------------------
int main( int argc, char *argv[] )
{
	// GLUT
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH );
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );
	glutInitWindowSize( 500, 500 );
	glutCreateWindow( "Programownie grafiki w OpenGL" );

	// -----------------------------
	// NOWE: Handlery
	// -----------------------------

	glutDisplayFunc( DisplayScene );
	glutReshapeFunc( Reshape );
	glutMouseFunc( MouseButton );
	glutMotionFunc( MouseMotion );
	glutMouseWheelFunc( MouseWheel );
	glutKeyboardFunc( KeyDown );
	glutKeyboardUpFunc( KeyUp );
	glutSpecialFunc( SpecialKeys );


	// GLEW
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if( GLEW_OK != err )
	{
		printf("GLEW Error\n");
		exit(1);
	}

	// OpenGL
	if( !GLEW_VERSION_3_2 )
	{
		printf("Brak OpenGL 3.2!\n");
		exit(1);
	}


	Initialize();

       // glutTimerFunc (100 , Animation , 0);
	glutTimerFunc ((int)(1000/60) , Animation ,0);
	glutMainLoop (); 



	// Cleaning
	glDeleteProgram(prog_no_light);
	glDeleteProgram(prog_phong);
	glDeleteProgram(prog_gouraud);
	glDeleteProgram(prog_distance);
	glDeleteProgram(prog_fog);

	return 0;
}
