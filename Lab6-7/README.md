# Projekt

* Poruszanie się klawiszami WSAD oraz góra/dół Spacja/V
* Klawisze 1-5 zmieniają oświetlenie

## Lab 6:

* Zadanie 1 **Implemented** 
    + klawisze 1, 2, 3 (no light, phong, guorand) zmieniają oświetlenie
    <img src="https://gitlab.com/opengl-2020/gustaw-napiorkowski/-/raw/master/Lab6-7/l6z1.png" width="250">
* Zadanie 2 **Implemented** 
    + kwiatki widoczne po prawej stronie sceny
    + kwiatki zawsze mają oświetlenie typu ambient
    <img src="https://gitlab.com/opengl-2020/gustaw-napiorkowski/-/raw/master/Lab6-7/l6z2.png" width="250">
* Zadanie 3 **Implemented** 
    + klawisz 4 włącza oświetlenie zależne od odległości
    + rząd sześcianów ma zawsze oświetlenie typu gourand-distance
    <img src="https://gitlab.com/opengl-2020/gustaw-napiorkowski/-/raw/master/Lab6-7/l6z3.png" width="250">
* Zadanie 4 **Implemented** 
    + klawisz 5 włącza oświetlenie typu phong wraz z mgłą
    <img src="https://gitlab.com/opengl-2020/gustaw-napiorkowski/-/raw/master/Lab6-7/l6z4.png" width="250">
* Zadanie 5 ***not implemented***
* Zadanie 6 ***not implemented***
* Zadanie 7 ***not implemented***

