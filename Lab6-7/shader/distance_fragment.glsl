#version 150 core
// in vec3 inoutNormal;
in vec2 inoutUV;

// uniform vec3 color_base;
uniform sampler2D tex0;

// kolor wejsciowy uwzglednia oswietlenie
// w modelu Gourauda obliczone
// w vertex shaderze
in vec3 ourColor;

out vec4 outColor;

void main()
{

	// ustawienie domyslnego koloru fragmentu
	// mozna tutaj uwzglednic tekstury i inne parametry
	// vec4 objectColor = vec4(color_base, 1.0f);
    // vec4 objectColor = vec4(0.2f, 0.8f, 0.2f, 1.0f);
    vec4 objectColor = texture( tex0, inoutUV ) ;// + vec4(inoutColor, 1.0) ;

	outColor = vec4(ourColor, 1.0f) * objectColor;

}
