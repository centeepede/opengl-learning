#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

// NOWE ! Biblioteki GLM do operacji na macierzach
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_stuff.h"
#include "teapot.h"

 
glm:: mat4 matProjection;
glm:: mat4 matView;

float RotationY = 0.1f;
float RotationX = 0.1f;
int window_width = 800;
int window_height = 800;
float brightness = 0.0;
// ---------------------------------------
// Identyfikatory obiektow

GLuint idProgram;	// programu
GLuint idVAO;		// tablic wierzcholkow
GLuint idVBO_coord;	// bufora wierzcholkow
GLuint idVBO_index;

GLuint idProgram2;	// programu
GLuint idVAO2;		// tablic wierzcholkow
GLuint idVBO_coord2;	// bufora wierzcholkow
GLuint idVBO_index2;

// ---------------------------------------
void DisplayScene()
{

	glClear( GL_COLOR_BUFFER_BIT );

    matView = glm::mat4(1.0f);
    matView = glm::translate(matView, glm::vec3(0.0, 0.0, -5.0));
    matView = glm::rotate(matView, RotationY, glm::vec3(1.0, 0.0, 0.0));
    matView = glm::rotate(matView, RotationX, glm::vec3(0.0, 1.0, 0.0));


//------------ I ------------------

	glViewport(0, 0, window_width/2, window_height/2);

	// Wlaczenie VAO i programu
	glBindVertexArray( idVAO );
	glUseProgram( idProgram );

	// Przekazanie zmiennej typu uniform
    glUniformMatrix4fv( glGetUniformLocation(idProgram , "matProjection"), 1, GL_FALSE, &matProjection[0][0] );
    glUniformMatrix4fv( glGetUniformLocation(idProgram , "matView"), 1, GL_FALSE, &matView[0][0] );
	glUniform1f(glGetUniformLocation(idProgram, "brightness"), brightness);

	// Generowanie obiektow na ekranie
	glDrawElements(GL_TRIANGLES, TEAPOT_INDICES_COUNT*3, GL_UNSIGNED_INT, NULL);


	// Wylaczanie
	glUseProgram( 0 );
	glBindVertexArray( 0 );

//------------ II ------------------

	glViewport(window_width/2, 0, window_width/2, window_height/2);

	// Wlaczenie VAO i programu
	glBindVertexArray( idVAO );
	glUseProgram( idProgram );

	// Przekazanie zmiennej typu uniform
    glUniformMatrix4fv( glGetUniformLocation(idProgram , "matProjection"), 1, GL_FALSE, &matProjection[0][0] );
    glUniformMatrix4fv( glGetUniformLocation(idProgram , "matView"), 1, GL_FALSE, &matView[0][0] );
	glUniform1f(glGetUniformLocation(idProgram, "brightness"), brightness);

	// Generowanie obiektow na ekranie
	glDrawElements(GL_LINES_ADJACENCY, TEAPOT_INDICES_COUNT*3, GL_UNSIGNED_INT, NULL);


	// Wylaczanie
	glUseProgram( 0 );
	glBindVertexArray( 0 );


//------------ III ------------------

	glViewport(0, window_height/2, window_width/2, window_height/2);

	// Wlaczenie VAO i programu
	glBindVertexArray( idVAO );
	glUseProgram( idProgram );

	// Przekazanie zmiennej typu uniform
    glUniformMatrix4fv( glGetUniformLocation(idProgram , "matProjection"), 1, GL_FALSE, &matProjection[0][0] );
    glUniformMatrix4fv( glGetUniformLocation(idProgram , "matView"), 1, GL_FALSE, &matView[0][0] );
	glUniform1f(glGetUniformLocation(idProgram, "brightness"), brightness);

	// Generowanie obiektow na ekranie
	glDrawElements(GL_POINTS, TEAPOT_INDICES_COUNT*3, GL_UNSIGNED_INT, NULL);


	// Wylaczanie
	glUseProgram( 0 );
	glBindVertexArray( 0 );

//------------ IV ------------------

	glViewport(window_width/2, window_height/2, window_width/2, window_height/2);

	// Wlaczenie VAO i programu
	glBindVertexArray( idVAO2 );
	glUseProgram( idProgram2 );

	// Przekazanie zmiennej typu uniform
    glUniformMatrix4fv( glGetUniformLocation(idProgram2 , "matProjection"), 1, GL_FALSE, &matProjection[0][0] );
    glUniformMatrix4fv( glGetUniformLocation(idProgram2 , "matView"), 1, GL_FALSE, &matView[0][0] );
	glUniform1f(glGetUniformLocation(idProgram2, "brightness"), brightness);

	// Generowanie obiektow na ekranie
	glDrawElements(GL_TRIANGLES, TEAPOT_INDICES_COUNT*3, GL_UNSIGNED_INT, NULL);


	// Wylaczanie
	glUseProgram( 0 );
	glBindVertexArray( 0 );
	glutSwapBuffers();
}

// ---------------------------------------
void Initialize()
{
	//glEnable(GL_DEPTH_TEST);
	glEnable(GL_DEPTH_TEST);
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );


	// 1. Program i shadery
	idProgram = glCreateProgram();

	glAttachShader( idProgram, LoadShader(GL_VERTEX_SHADER, "vertex.glsl"));
	glAttachShader( idProgram, LoadShader(GL_FRAGMENT_SHADER, "fragment.glsl"));

	LinkAndValidateProgram( idProgram );


	// 2. Vertex arrays
	glGenVertexArrays( 1, &idVAO );
	glBindVertexArray( idVAO );

	// Bufor na wspolrzedne wierzcholkow
	glGenBuffers( 1, &idVBO_coord );

	glBindBuffer( GL_ARRAY_BUFFER, idVBO_coord );
	glBufferData( GL_ARRAY_BUFFER, sizeof(teapotPosition), teapotPosition, GL_STATIC_DRAW );

	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
	glEnableVertexAttribArray( 0 );



	// Bufor na kolory wierzcholkow
	// glGenBuffers( 1, &idVBO_color );

	// glBindBuffer( GL_ARRAY_BUFFER, idVBO_color );
	// glBufferData( GL_ARRAY_BUFFER, sizeof(teapotNormal), teapotNormal, GL_STATIC_DRAW );

	// glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, NULL );
	// glEnableVertexAttribArray( 1 );


	// Bufor na indeksy wierzcholkow
    glGenBuffers( 1, &idVBO_index );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, idVBO_index );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(teapotIndices), teapotIndices, GL_STATIC_DRAW );


	glBindVertexArray( 0 );

	// 1. Program i shadery
	idProgram2 = glCreateProgram();

	glAttachShader( idProgram2, LoadShader(GL_VERTEX_SHADER, "vertex2.glsl"));
	glAttachShader( idProgram2, LoadShader(GL_FRAGMENT_SHADER, "fragment2.glsl"));

	LinkAndValidateProgram( idProgram2 );


	// 2. Vertex arrays
	glGenVertexArrays( 1, &idVAO2 );
	glBindVertexArray( idVAO2 );

	// Bufor na wspolrzedne wierzcholkow
	glGenBuffers( 1, &idVBO_coord2 );

	glBindBuffer( GL_ARRAY_BUFFER, idVBO_coord2 );
	glBufferData( GL_ARRAY_BUFFER, sizeof(teapotPosition), teapotPosition, GL_STATIC_DRAW );

	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
	glEnableVertexAttribArray( 0 );



	// Bufor na kolory wierzcholkow
	// glGenBuffers( 1, &idVBO_color );

	// glBindBuffer( GL_ARRAY_BUFFER, idVBO_color );
	// glBufferData( GL_ARRAY_BUFFER, sizeof(teapotNormal), teapotNormal, GL_STATIC_DRAW );

	// glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, NULL );
	// glEnableVertexAttribArray( 1 );


	// Bufor na indeksy wierzcholkow
    glGenBuffers( 1, &idVBO_index2 );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, idVBO_index2 );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(teapotIndices), teapotIndices, GL_STATIC_DRAW );


	glBindVertexArray( 0 );
}

// ---------------------------------------
void Reshape( int width, int height )
{
	window_width = width;
	window_height = height;
	matProjection = glm::perspective(90.0f, width/(float)height, 0.01f, 10.0f);
}

// ---------------------------------------------------
void Keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27: // ESC key
		exit(0);
		break;
	case 'e':
		brightness += 0.1;
		break;
	case 'q':
		brightness -= 0.1;
		break;
	case 'a':
		RotationX += 0.1;
		break;
	case 'd':
		RotationX -= 0.1;
		break;
	case 'w':
		RotationY += 0.1;
		break;
	case 's':
		RotationY -= 0.1;
		break;
	}

    glutPostRedisplay();
}


// ---------------------------------------------------
int main( int argc, char *argv[] )
{
	// GLUT
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB );
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );
	glutInitWindowSize( 500, 500 );
	glutCreateWindow( "OpenGL!" );
	glutDisplayFunc( DisplayScene );
	glutReshapeFunc( Reshape );

	// Keyboard
	glutKeyboardFunc( Keyboard );


	// GLEW
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if( GLEW_OK != err )
	{
		printf("GLEW Error\n");
		exit(1);
	}

	// OpenGL
	if( !GLEW_VERSION_3_2 )
	{
		printf("Brak OpenGL 3.2!\n");
		exit(1);
	}


	Initialize();


	glutMainLoop();


	// Cleaning
	glDeleteProgram( idProgram );
	glDeleteVertexArrays( 1, &idVBO_coord );
	glDeleteVertexArrays( 1, &idVAO );

	return 0;
}
