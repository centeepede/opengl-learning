// Fragment shader
#version 330

// Predefiniowane dane wejsciowe
//in  vec4 gl_FragCoord;
//in  int  gl_PrimitiveID;

// Dane wyjsciowe
out vec4 outColor;

// flat in vec3 fragColor; <- wylaczenie interpolacji
// in vec3 fragColor;

uniform float brightness;

void main()
{
    outColor = vec4(1.0, 1.0, 0.0, 1.0);

	outColor += vec4(vec3(brightness), 0.0);
}
