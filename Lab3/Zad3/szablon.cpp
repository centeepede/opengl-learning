#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

// NOWE ! Biblioteki GLM do operacji na macierzach
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_stuff.h"
#include "teapot.h"

 
glm:: mat4 matProjection;
glm:: mat4 matView;

float RotationY = 0.1f;
// ---------------------------------------
// Identyfikatory obiektow

GLuint idProgram;	// programu
GLuint idVAO;		// tablic wierzcholkow
GLuint idVBO_coord;	// bufora wierzcholkow
GLuint idVBO_color;
GLuint idVBO_index;



// #define NUMBER_OF_TRIANGLES	12

// // Wspolrzedne wierzchokow
// GLfloat triangles_coord[8*3] =
// {
    // // All faces
    // 0.5f, 0.5f, 0.5f,
    // 0.5f, 0.5f,-0.5f,
    // 0.5f,-0.5f, 0.5f,
    // 0.5f,-0.5f,-0.5f,
    // -0.5f, 0.5f, 0.5f,
    // -0.5f, 0.5f,-0.5f,
    // -0.5f,-0.5f, 0.5f,
    // -0.5f,-0.5f,-0.5f

// };


// // Kolory wierzcholkow
// GLfloat triangles_color[8*3] =
// {
    // 0.5f, 0.5f, 0.5f,
    // 0.5f, 0.5f,1.0f,
    // 0.5f,1.0f, 0.5f,
    // 0.5f,1.0f,1.0f,
    // 1.0f, 0.5f, 0.5f,
    // 1.0f, 0.5f,1.0f,
    // 1.0f,1.0f, 0.5f,
    // 1.0f,1.0f,1.0f
// };
// // Indeksy wierzcholkow
// GLuint triangles_indices[3*NUMBER_OF_TRIANGLES] =
// {
    // 1, 5, 4,
    // 4, 0, 1,
    // 2, 6, 7,
    // 7, 3, 2,
    // 0, 4, 6,
    // 6, 2, 0,
    // 3, 7, 5,
    // 5, 1, 3,
    // 4, 5, 7,
    // 7, 6, 4,
    // 1, 0, 2,
    // 2, 3, 1
// };

// ---------------------------------------
void DisplayScene()
{

	glClear( GL_COLOR_BUFFER_BIT );

    matView = glm::mat4(1.0f);
    matView = glm::translate(matView, glm::vec3(0.0, 0.0, -5.0));
    matView = glm::rotate(matView, RotationY, glm::vec3(0.0, 1.0, 0.0));


	// glViewport(0, 0, window_width, window_height);

	// Wlaczenie VAO i programu
	glBindVertexArray( idVAO );
	glUseProgram( idProgram );

	// Przekazanie zmiennej typu uniform
    glUniformMatrix4fv( glGetUniformLocation(idProgram , "matProjection"), 1, GL_FALSE, &matProjection[0][0] );
    glUniformMatrix4fv( glGetUniformLocation(idProgram , "matView"), 1, GL_FALSE, &matView[0][0] );

	// Generowanie obiektow na ekranie
	// glDrawArrays( GL_TRIANGLES, 0, NUMBER_OF_TRIANGLES*3 );
	glDrawElements(GL_TRIANGLES, TEAPOT_INDICES_COUNT*3, GL_UNSIGNED_INT, NULL);


	// Wylaczanie
	glUseProgram( 0 );
	glBindVertexArray( 0 );


	glutSwapBuffers();
}

// ---------------------------------------
void Initialize()
{
	//glEnable(GL_DEPTH_TEST);
	glEnable(GL_DEPTH_TEST);
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );


	// 1. Program i shadery
	idProgram = glCreateProgram();

	glAttachShader( idProgram, LoadShader(GL_VERTEX_SHADER, "vertex.glsl"));
	glAttachShader( idProgram, LoadShader(GL_FRAGMENT_SHADER, "fragment.glsl"));

	LinkAndValidateProgram( idProgram );


	// 2. Vertex arrays
	glGenVertexArrays( 1, &idVAO );
	glBindVertexArray( idVAO );

	// Bufor na wspolrzedne wierzcholkow
	glGenBuffers( 1, &idVBO_coord );

	glBindBuffer( GL_ARRAY_BUFFER, idVBO_coord );
	glBufferData( GL_ARRAY_BUFFER, sizeof(teapotPosition), teapotPosition, GL_STATIC_DRAW );

	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
	glEnableVertexAttribArray( 0 );



	// Bufor na kolory wierzcholkow
	// glGenBuffers( 1, &idVBO_color );

	// glBindBuffer( GL_ARRAY_BUFFER, idVBO_color );
	// glBufferData( GL_ARRAY_BUFFER, sizeof(teapotNormal), teapotNormal, GL_STATIC_DRAW );

	// glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, NULL );
	// glEnableVertexAttribArray( 1 );


	// Bufor na indeksy wierzcholkow
    glGenBuffers( 1, &idVBO_index );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, idVBO_index );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(teapotIndices), teapotIndices, GL_STATIC_DRAW );


	glBindVertexArray( 0 );
}

// ---------------------------------------
void Reshape( int width, int height )
{
	glViewport( 0, 0, width, height );

	// Macierze projekcji (ortogonalna i perspektywiczna)
    // matProj = glm::ortho(-1.0, 1.0, -1.0, 1.0);
    matProjection = glm::perspective(glm::radians(90.0f), width/(float)height, 0.01f, 5.0f);
}

// ---------------------------------------------------
void Keyboard( unsigned char key, int x, int y )
{
    switch (key)
    {
		case 27:	// ESC key
			exit(0);
			break;

		case 'w':
            RotationY += 0.1f;
			break;

		case 's':
            RotationY -= 0.1f;
			break;

		case 'd':
			break;

		case 'a':
			break;

    }

    glutPostRedisplay();
}


// ---------------------------------------------------
int main( int argc, char *argv[] )
{
	// GLUT
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB );
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );
	glutInitWindowSize( 500, 500 );
	glutCreateWindow( "OpenGL!" );
	glutDisplayFunc( DisplayScene );
	glutReshapeFunc( Reshape );

	// Keyboard
	glutKeyboardFunc( Keyboard );


	// GLEW
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if( GLEW_OK != err )
	{
		printf("GLEW Error\n");
		exit(1);
	}

	// OpenGL
	if( !GLEW_VERSION_3_2 )
	{
		printf("Brak OpenGL 3.2!\n");
		exit(1);
	}


	Initialize();


	glutMainLoop();


	// Cleaning
	glDeleteProgram( idProgram );
	glDeleteVertexArrays( 1, &idVBO_coord );
	glDeleteVertexArrays( 1, &idVAO );

	return 0;
}
