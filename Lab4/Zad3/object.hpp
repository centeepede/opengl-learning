#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_stuff.hpp"
#include "obj_loader.hpp"

class Object {

    private:
        glm::mat4 _matObject;
        float _object_rotate_x = 0.0f;
        float _object_rotate_y = 0.0f;
        float _object_rotate_z = 0.0f;
        float _object_translate_x = 0.0f;
        float _object_translate_y = 0.0f;
        float _object_translate_z = 0.0f;
        float _color[3];

        std::vector<glm::vec3> _OBJ_vertices;
        std::vector<glm::vec2> _OBJ_uvs;
        std::vector<glm::vec3> _OBJ_normals;

    public:
        void init(const char * fileName, float r = 0.5f, float g = 0.5f, float b = 0.5f)
        {
            if (!loadOBJ(fileName, _OBJ_vertices, _OBJ_uvs, _OBJ_normals))
            {
                printf("Not loaded!\n");
                exit(1);
            }
            _color[0] = r;
            _color[1] = g;
            _color[2] = b;
            printf("My colors: ");
            printf("r%f", _color[0]);
            printf("g%f", _color[1]);
            printf("b%f\n", _color[2]);
        }

        glm::mat4 getView(glm::mat4 matView)
        {

            _matObject = glm::mat4x4( 1.0 );
            _matObject = glm::translate( matView, glm::vec3( _object_translate_x, _object_translate_y, _object_translate_z) );
            _matObject = glm::rotate( _matObject, _object_rotate_y, glm::vec3( 0.0f, 1.0f, 0.0f ) );
            _matObject = glm::rotate( _matObject, _object_rotate_x, glm::vec3( 1.0f, 0.0f, 0.0f ) );
            _matObject = glm::rotate( _matObject, _object_rotate_z, glm::vec3( 0.0f, 0.0f, 1.0f ) );

            return(_matObject);
        }

        void rotate(float x = 0.0f, float y = 0.0f, float z = 0.0f)
        {
            _object_rotate_x += x;
            _object_rotate_y += y;
            _object_rotate_z += z;
        }

        void translate(float x = 0.0f, float y = 0.0f, float z = 0.0f)
        {
            _object_translate_x += x;
            _object_translate_y += y;
            _object_translate_z += z;
        }

        float r()
        {
            return _color[0];
        }

        float g()
        {
            return _color[1];
        }

        float b()
        {
            return _color[2];
        }

        std::vector<glm::vec3> OBJ_vertices()
        {
            return _OBJ_vertices;
        }
        std::vector<glm::vec2> OBJ_uvs()
        {
            return _OBJ_uvs;
        }
        std::vector<glm::vec3> OBJ_normals()
        {
            return _OBJ_normals;
        }
        
        const char * vertexglsl()
        {
            return "vertex.glsl";
        }
        const char * fragmentglsl()
        {
            return "fragment.glsl";
        }
};
