#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_stuff.hpp"
#include "obj_loader.hpp"
#include "object.hpp"

#define OBJECT_COUNT 4


glm::mat4 matProj;
glm::mat4 matView;
glm::mat4 matProjView;


// Identyfikatory obiektow

GLuint idProgram[OBJECT_COUNT];	// programu
GLuint idVAO[OBJECT_COUNT];		// tablic wierzcholkow
GLuint idVBO_coord[OBJECT_COUNT];	// bufora na wspolrzedne
GLuint idVBO_color[OBJECT_COUNT]; // bufora na kolory
GLuint idVBO_normal[OBJECT_COUNT]; // bufora na kolory

Object objects[OBJECT_COUNT];

// ---------------------------------------
void DisplayScene()
{

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // Macierz widoku
    matView = glm::mat4x4( 1.0 );
    matView = glm::translate( matView, glm::vec3( _scene_translate_x, _scene_translate_y, _scene_translate_z) );
    matView = glm::rotate( matView, _scene_rotate_x, glm::vec3( 1.0f, 0.0f, 0.0f ) );
    matView = glm::rotate( matView, _scene_rotate_y, glm::vec3( 0.0f, 1.0f, 0.0f ) );
    // --------------- I --------------------
    for (int i = 0; i < OBJECT_COUNT; i++)
    {


        // Obliczanie macierzy rzutowania
        matProjView = matProj * objects[i].getView(matView);


        // Wlaczenie VAO i programu
        glBindVertexArray( idVAO[i] );
        glUseProgram( idProgram[i] );

		// Przekazanie macierzy rzutowania
		glUniformMatrix4fv( glGetUniformLocation(idProgram[i], "matProjView"), 1, GL_FALSE,  glm::value_ptr(matProjView));

	glUniform3f(glGetUniformLocation(idProgram[i], "color_base"), objects[i].r(), objects[i].g(), objects[i].b() );
		// Generowanie obiektow na ekranie
		glDrawArrays( GL_TRIANGLES, 0, objects[i].OBJ_vertices().size() );
        

    }
	// Wylaczanie
	glUseProgram( 0 );
	glBindVertexArray( 0 );


	glutSwapBuffers();

}

// ---------------------------------------
void Initialize()
{

	_scene_translate_z = -10.0f;

	glClearColor( 0.2f, 0.2f, 0.2f, 1.0f );

	glEnable(GL_DEPTH_TEST);

    for (int i = 0; i < OBJECT_COUNT; i++)
    {
        // objects[i].init("blended.obj");
        // printf("file no. %d", i);
        switch (i)
        {
            case 0:
                objects[i].init("base.obj", 0.5f, 0.5f, 0.5f);
                break;
            case 1:
                objects[i].init("humans.obj", 0.8f, 0.4f, 0.2f);
                break;
            case 2:
                objects[i].init("bride_clothes.obj", 1.0f, 1.0f, 1.0f);
                break;
            case 3:
                objects[i].init("groom_clothes.obj", 0.0f, 0.0f, 0.1f);
                break;
            // default:
                // objects[i].init("blended.obj");
        }


        // 1. Program i shadery
        idProgram[i] = glCreateProgram();

        printf("shader no.%d \n", i);
        switch (i)
        {
            case 0:
                printf("vert...");
                glAttachShader( idProgram[i], LoadShader(GL_VERTEX_SHADER, "vertex.glsl"));
                printf("frag...");
                glAttachShader( idProgram[i], LoadShader(GL_FRAGMENT_SHADER, "fragment.glsl"));
                break;
            case 1:
                printf("vert...");
                glAttachShader( idProgram[i], LoadShader(GL_VERTEX_SHADER, "vertex2.glsl"));
                printf("frag...");
                glAttachShader( idProgram[i], LoadShader(GL_FRAGMENT_SHADER, "fragment2.glsl"));
                break;
            case 2:
                printf("vert...");
                glAttachShader( idProgram[i], LoadShader(GL_VERTEX_SHADER, "vertex2.glsl"));
                printf("frag...");
                glAttachShader( idProgram[i], LoadShader(GL_FRAGMENT_SHADER, "fragment3.glsl"));
                break;
            case 3:
                printf("vert...");
                glAttachShader( idProgram[i], LoadShader(GL_VERTEX_SHADER, "vertex4.glsl"));
                printf("frag...");
                glAttachShader( idProgram[i], LoadShader(GL_FRAGMENT_SHADER, "fragment4.glsl"));
                break;
        }
        // glAttachShader( idProgram[i], LoadShader(GL_VERTEX_SHADER, "vertex.glsl"));
        // glAttachShader( idProgram[i], LoadShader(GL_FRAGMENT_SHADER, "fragment.glsl"));

        LinkAndValidateProgram( idProgram[i] );


        // 2. Vertex arrays
        glGenVertexArrays( 1, &idVAO[i] );
        glBindVertexArray( idVAO[i] );

        // Bufor na wspolrzedne wierzcholkow
        glGenBuffers( 1, &idVBO_coord[i] );

        glBindBuffer( GL_ARRAY_BUFFER, idVBO_coord[i] );
        glBufferData( GL_ARRAY_BUFFER, objects[i].OBJ_vertices().size() * sizeof(glm::vec3), &objects[i].OBJ_vertices()[0], GL_STATIC_DRAW );

        glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
        glEnableVertexAttribArray( 0 );

        // Bufor na wspolrzedne wierzcholkow
        glGenBuffers( 1, &idVBO_normal[i] );

        glBindBuffer( GL_ARRAY_BUFFER, idVBO_normal[i] );
        glBufferData( GL_ARRAY_BUFFER, objects[i].OBJ_normals().size() * sizeof(glm::vec3), &objects[i].OBJ_normals()[0], GL_STATIC_DRAW );

        glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, NULL );
        glEnableVertexAttribArray( 1 );

    }

    glBindVertexArray( 0 );


}

// ---------------------------------------
void Reshape( int width, int height )
{
	glViewport( 0, 0, width, height );

	matProj = glm::ortho(-1.0, 1.0, -1.0, 1.0);
	matProj = glm::perspective(glm::radians(80.0f), width/(float)height, 0.1f, 20.0f);

}




// ---------------------------------------------------
int main( int argc, char *argv[] )
{
	// GLUT
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );
	glutInitWindowSize( 500, 500 );
	glutCreateWindow( "Programownie grafiki w OpenGL" );

	// -----------------------------
	// NOWE: Handlery
	// -----------------------------

	glutDisplayFunc( DisplayScene );
	glutReshapeFunc( Reshape );
	glutMouseFunc( MouseButton );
	glutMotionFunc( MouseMotion );
	glutMouseWheelFunc( MouseWheel );
	glutKeyboardFunc( Keyboard );
	glutSpecialFunc( SpecialKeys );


	// GLEW
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if( GLEW_OK != err )
	{
		printf("GLEW Error\n");
		exit(1);
	}

	// OpenGL
	if( !GLEW_VERSION_3_2 )
	{
		printf("Brak OpenGL 3.2!\n");
		exit(1);
	}


	Initialize();


	glutMainLoop();


	// Cleaning
    for (int i = 0; i < OBJECT_COUNT; i++)
    {
	glDeleteProgram( idProgram[i] );
	glDeleteVertexArrays( 1, &idVBO_coord[i] );
	glDeleteVertexArrays( 1, &idVAO[i] );
    }

	return 0;
}
