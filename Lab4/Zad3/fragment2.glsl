#version 150

in vec4 inoutPos;
in vec3 inoutNormal;
out vec4 outColor;

uniform vec3 color_base;


void main()
{

    // odleglosc od poczatku ukladu wspolrzednych
    float odl = sqrt(inoutPos.x*inoutPos.x + inoutPos.y*inoutPos.y + inoutPos.z*inoutPos.z);

    // szalone kolory
    vec3 crazyColor = vec3(
        sin(odl*3.0f)/2.0f + 0.5f,
        sin(inoutPos.y)/2.0f + 0.5f,
        sin(gl_PrimitiveID/100.0)/5.0f + 0.5f
    );
    float l = inoutNormal.r+inoutNormal.g+inoutNormal.b;

	vec3 myColor = vec3(l, l, l) * color_base;

	outColor = vec4(myColor, 1.0);
}
