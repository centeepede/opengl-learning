#version 330

// Wspolrzedne wierzcholkow
layout( location = 0 ) in vec4 inPosition;
layout( location = 1 ) in vec3 inNormal;

// Macierz rzutowania
uniform mat4 matProjView;


out vec4 inoutPos;
out vec3 inoutNormal;

void main()
{
    inoutNormal = inNormal;

	gl_Position = matProjView * inPosition;
	inoutPos = inPosition;
}
