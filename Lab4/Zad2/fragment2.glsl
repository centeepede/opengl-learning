#version 150

in vec4 inoutPos;
in vec3 inoutNormal;
out vec4 outColor;

uniform float color_head;


void main()
{

    // odleglosc od poczatku ukladu wspolrzednych
    float odl = sqrt(inoutPos.x*inoutPos.x + inoutPos.y*inoutPos.y + inoutPos.z*inoutPos.z);

    // szalone kolory
    vec3 crazyColor = vec3(
        sin(odl*3.0f)/2.0f + 0.5f,
        sin(inoutPos.y)/2.0f + 0.5f,
        sin(gl_PrimitiveID/100.0)/5.0f + 0.5f
    );

	// vec3 myColor = inoutNormal;
	vec3 myColor = inoutNormal * vec3(color_head, 1.0f, color_head);

	outColor = vec4(myColor, 1.0);
}
