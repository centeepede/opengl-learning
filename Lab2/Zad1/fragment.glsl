// Fragment shader
#version 330


// Predefiniowane dane wejsciowe ---> initialized by default. This init would break the program
//in  vec4 gl_FragCoord;
//in  int  gl_PrimitiveID;

float random (vec2 uv)
{
	return sin(dot(uv,vec2(12.9898,78.233)))*43758.5453123
	  - floor(sin(dot(uv,vec2(12.9898,78.233)))*43758.5453123);
}

// Dane wyjsciowe
out vec4 outColor;


void main()
{

	outColor = vec4(random(vec2(gl_FragCoord.x, gl_FragCoord.y)), random(vec2(gl_FragCoord.x, gl_FragCoord.z)), random(vec2(gl_FragCoord.y, gl_FragCoord.x)), 1.0);

}
