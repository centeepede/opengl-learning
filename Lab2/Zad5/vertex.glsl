// Vertex shader
#version 330

// Dane pobrane z VAO
layout (location = 0) in vec4 inPosition;
layout (location = 1)in vec3 inColor;


// Predefiniowane dane wejsciowe
//in int gl_VertexID;
//in int gl_InstanceID;

// Dane wyjsciowe (kolejny etap potoku)
//out vec4 gl_Position;

out vec3 fragColor;

uniform mat4 trans;

void main()
{


    fragColor = inColor;


	// finalna pozycja wierzcholka
    // gl_Position = trans * finalPosition;
    gl_Position = vec4(
        inPosition.y,
        -inPosition.x,
        inPosition.z,
        1.0f
    );
}
