# Struktura:

* Foldery Lab1 - LabN -> zadania z poszczególnych zajęć
* Folder projekt -> najnowsza wersja (kopia ostatnich Lab z ewentualnymi zmianami na przyszłe Lab)

# Licencja:

Autor Gustaw Napiórkowski udostępnia kod źródłowy pod licencją
![MIT](LICENSE) z wyłączeniem fragmentów, które nie zostały wykonane
przez niego, a w szczególności bibliotekę stb_image wykorzystaną w tym projekcie.
Modele 3D i tekstury do nich dołączone wykonane przez siebie udostępnia pod licenją
![Cretive Commons Share-Alike 4.0](LICENSE-models)

# Atrybucje:

W projekcie został wykorzystany model "blenderowej małpki"
wykonany przez Willem-Paul van Overbruggen oraz modele i tekstry kwiatków,
tekstura lasu nocą Lab5/Zad3/background.png nie do końca znanego pochodzenia,
ale udostępnione przez prowadzącego zajęcia. Nie są one w związku z tym objęte
wyżej sprecyzowaną licencją.

