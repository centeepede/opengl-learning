# Projekt

* Poruszanie się klawiszami WSAD oraz góra/dół Spacja/V
* Prawy przycisk myszki włącza Menu z opcjami

## Lab8:

* Zadanie 1 **Implemented**
    + Prawy przycisk myszki włącza menu => przełączanie światła z Lab6 zmienione
* Zadanie 2 **Implemented** skybox widoczny w projekcie w tle
* Zadanie 3 **Implemented** Głowa małpy widoczna na przeciwko głównej sceny
    <img src="https://gitlab.com/opengl-2020/gustaw-napiorkowski/-/raw/master/Lab8/l8z1-3.png" alt="zad1-3" width="250">
* Zadanie 4 **Implemented** Fps po lewej na dole
* Zadanie 5 **Implemented** klasa TextMessage w cfont.hpp. Fps i instrukcja poruszania widoczna w innych czcionkach
    <img src="https://gitlab.com/opengl-2020/gustaw-napiorkowski/-/blob/master/Lab8/l8z4-5.png" alt="zad4-5" width="250">
* Zadanie 6 (optional) ***not implemented***
