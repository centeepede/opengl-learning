#include <ctime>
#include "shader_stuff.hpp"
// #include "definitions.hpp"

int frame = 0;
int fps = 60;
time_t previous_time = time(NULL);
//---------------------------------------
void Animation(int keyframe)
{
    frame ++;
    if (previous_time + 1 <= time(NULL))
    {
        previous_time = time(NULL);
        fps = frame;
        frame = 0;
    }

    if (keys[27])
    {
        exit(0);
    }

    //movement
    const float MOVE = 0.05f;

    float dir_mod_z = 0.0f;
    float dir_mod_x = 0.0f;

    if ( keys ['w'] )
    {
        dir_mod_z += MOVE*cos(-_scene_rotate_y);
        dir_mod_x += MOVE*sin(-_scene_rotate_y);
    }
    if ( keys ['s'] )
    {
        dir_mod_z += -MOVE*cos(-_scene_rotate_y);
        dir_mod_x += -MOVE*sin(-_scene_rotate_y);
    }
    if ( keys ['a'] )
    {
        dir_mod_z += MOVE*sin(_scene_rotate_y);
        dir_mod_x += MOVE*cos(_scene_rotate_y);
    }
    if ( keys ['d'] )
    {
        dir_mod_z += -MOVE*sin(_scene_rotate_y);
        dir_mod_x += -MOVE*cos(_scene_rotate_y);
    }

    _scene_translate_z = dir_mod_z;
    _scene_translate_x = dir_mod_x;


    if ( keys ['v'] )
    {
        _scene_translate_y = MOVE;
    }else if ( keys [' '] )
    {
        _scene_translate_y = -MOVE;
    }else{
        _scene_translate_y = 0.0f;
    }


    // //LIGHT change
    // if ( keys ['1'] )
    // {
        // light_mode = NO_LIGHT;
    // }
    // if ( keys ['2'] )
    // {
        // light_mode = PHONG;
    // }
    // if ( keys ['3'] )
    // {
        // light_mode = GOURAUD;
    // }
    // if ( keys ['4'] )
    // {
        // light_mode = LIGHT_DISTANCE;
    // }
    // if ( keys ['5'] )
    // {
        // light_mode = LIGHT_FOG;
    // }

    glutPostRedisplay ();
	glutTimerFunc ((int)(1000/60) , Animation ,0);
}


