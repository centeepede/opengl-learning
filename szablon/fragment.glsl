#version 330


// Predefiniowane dane wejsciowe
//in  vec4 gl_FragCoord;
//in  int  gl_PrimitiveID;


// Dane wyjsciowe
out vec4 outColor;

float random (vec2 uv)
{
	return sin(dot(uv,vec2(12.9898,78.233)))*43758.5453123
	  - floor(sin(dot(uv,vec2(12.9898,78.233)))*43758.5453123);
}

void main()
{

	if (gl_PrimitiveID%2 == 0)
		outColor = vec4(random(vec2(1.0, 2.0)), 0.0, 0.0, 1.0);
	else
		outColor = vec4(0.0, 1.0, 0.0, 1.0);

}
