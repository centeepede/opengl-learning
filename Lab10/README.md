# Projekt

* Poruszanie się klawiszami WSAD oraz góra/dół Spacja/V
* Klawisze 1-5 zmieniają oświetlenie

## Lab 6:

***znajduje sie w folderze projektV0.1***

* Zadanie 1 **Implemented** 
    + klawisze 1, 2, 3 (no light, phong, guorand) zmieniają oświetlenie
* Zadanie 2 **Implemented** 
    + kwiatki widoczne po prawej stronie sceny
    + kwiatki zawsze mają oświetlenie typu ambient
* Zadanie 3 **Implemented** 
    + klawisz 4 włącza oświetlenie zależne od odległości
    + rząd sześcianów ma zawsze oświetlenie typu gourand-distance
* Zadanie 4 **Implemented** 
    + klawisz 5 włącza oświetlenie typu phong wraz z mgłą
* Zadanie 5 ***not implemented***
* Zadanie 6 ***not implemented***
* Zadanie 7 ***not implemented***

## Lab8:

* Zadanie 1 **Implemented**
    + Prawy przycisk myszki włącza menu => przełączanie światła z Lab6 zmienione
* Zadanie 2 **Implemented** skybox widoczny w projekcie w tle
* Zadanie 3 **Implemented** Głowa małpy widoczna na przeciwko głównej sceny
* Zadanie 4 **Implemented** Fps po lewej na dole
* Zadanie 5 **Implemented** klasa TextMessage w cfont.hpp. Fps i instrukcja poruszania widoczna w innych czcionkach
* Zadanie 6 (optional) ***not implemented***
