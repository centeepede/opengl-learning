#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_stuff.hpp"
#include "obj_loader.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "ground.h"

#include "texture_loader.hpp"
class Object {
    public:
        glm::mat4 _matObject;
        glm::mat4 matModel;
        glm::vec3 Position;
        float _color[3];
        std::vector<glm::vec3> OBJ_vertices;
        std::vector<glm::vec2> OBJ_uvs;
        std::vector<glm::vec3> OBJ_normals;
        GLuint idProgram;	// programu
        GLuint vArray;
        // GLuint vBuffer_coord;	// bufora na wspolrzedne
        // GLuint vBuffer_normal; // bufora na kolory
        // GLuint vBuffer_uv; // bufora na kolory
        GLuint TextureID;
        GLuint Size;
        CGround *Ground = NULL;


        glm::mat4 getView()
        {

            return( matModel * _matObject);
        }

        void rotate(float x = 0.0f, float y = 0.0f, float z = 0.0f)
        {
            _matObject = glm::rotate( _matObject, y, glm::vec3( 0.0f, 1.0f, 0.0f ) );
            _matObject = glm::rotate( _matObject, x, glm::vec3( 1.0f, 0.0f, 0.0f ) );
            _matObject = glm::rotate( _matObject, z, glm::vec3( 0.0f, 0.0f, 1.0f ) );
        }

        void rotate_absolute(float x = 0.0f, float y = 0.0f, float z = 0.0f)
        {
            _matObject = glm::rotate( _matObject, y, glm::vec3( 0.0f, 1.0f, 0.0f ) );
            _matObject = glm::rotate( _matObject, x, glm::vec3( 1.0f, 0.0f, 0.0f ) );
            _matObject = glm::rotate( _matObject, z, glm::vec3( 0.0f, 0.0f, 1.0f ) );
        }

        void translate(float x = 0.0f, float y = 0.0f, float z = 0.0f)
        {
            Position.x += x;
            Position.y += y;
            Position.z += z;
            this->SetPosition(Position);
            // _matObject = glm::translate( _matObject, glm::vec3( x, y, z) );
        }

        void scale(float s)
        {
            _matObject = glm::scale( _matObject, glm::vec3(s, s, s) );
        }

        void scale(float x, float y, float z)
        {
            _matObject = glm::scale( _matObject, glm::vec3( x, y, z) );
        }

        void draw()
        {
            glBindVertexArray( vArray );
            glDrawArrays( GL_TRIANGLES, 0, Size );
            glBindVertexArray( 0 );
        }

        void _init(const char * fileName, const char * texFName, float r = 0.5f, float g = 0.5f, float b = 0.5f)
        {
            // printf("1\n");
            _matObject = glm::mat4x4( 1.0 );
            Position = glm::vec3(0.0f, 0.0f, 0.0f);
            matModel = glm::translate(glm::mat4(1.0f), Position);

            // printf("2\n");
            CreateTexture(texFName);

            if (!loadOBJ(fileName, OBJ_vertices, OBJ_uvs, OBJ_normals))
            {
                printf("ERROR: Not loaded %s!\n", fileName);
                exit(1);
            }
            _color[0] = r;
            _color[1] = g;
            _color[2] = b;

            Size = OBJ_vertices.size();

            // Vertex arrays
            glGenVertexArrays( 1, &this->vArray );
            glBindVertexArray( this->vArray );

            // Wspolrzedne wierzchokow
            GLuint vBuffer_coord;
            glGenBuffers( 1, &vBuffer_coord );
            glBindBuffer( GL_ARRAY_BUFFER, vBuffer_coord );
            glBufferData( GL_ARRAY_BUFFER, OBJ_vertices.size() * sizeof(glm::vec3), &OBJ_vertices[0], GL_STATIC_DRAW );

            glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
            glEnableVertexAttribArray( 0 );

            // Wektory normalne
            GLuint vBuffer_normal;
            glGenBuffers( 1, &vBuffer_normal );
            glBindBuffer( GL_ARRAY_BUFFER, vBuffer_normal );
            glBufferData( GL_ARRAY_BUFFER, OBJ_normals.size() * sizeof(glm::vec3), &OBJ_normals[0], GL_STATIC_DRAW );

            glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, NULL );
            glEnableVertexAttribArray( 1 );

            // Wspolrzedne UV
            GLuint vBuffer_uv;
            glGenBuffers( 1, &vBuffer_uv );
            glBindBuffer( GL_ARRAY_BUFFER, vBuffer_uv );
            glBufferData( GL_ARRAY_BUFFER, OBJ_uvs.size() * sizeof(glm::vec2), &OBJ_uvs[0], GL_STATIC_DRAW );
            glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, 0, NULL );
            glEnableVertexAttribArray( 2 );

            glBindVertexArray( 0 );
        }

        void init(const char * fileName, const char * texFName, float r = 0.5f, float g = 0.5f, float b = 0.5f)
        {
            // printf("normal init\n");
            this->_init(fileName, texFName, r, g, b);
        }

        void init(const char * fileName, const char * texFName, CGround *_ground, float r = 0.5f, float g = 0.5f, float b = 0.5f )
        {
            // printf("weird init\n");
            Ground = _ground;
            this->_init(fileName, texFName, r, g, b);
        }

        void CreateTexture(const char * texFName)
        {
            int tex_width;
            int tex_height;
            int comp;
            auto rgb_mode = GL_RGBA;
            unsigned char* tex_data;

            if (strstr(texFName, ".bmp"))
            {
                loadBMP_custom(texFName, tex_width, tex_height, &tex_data);
                rgb_mode = GL_BGR;
            } else
            {
                tex_data = stbi_load(texFName, &tex_width, &tex_height, &comp, STBI_rgb_alpha);
                if (comp == 3)
                {
                    rgb_mode = GL_RGB;
                } 
                else if (comp == 4)
                {
                    rgb_mode = GL_RGBA;
                    // printf("found flower %s\n", texFName);
                } 
            }

            if(tex_data == NULL)
            {
                printf("ERROR: Texture\"%s\" not loaded!", texFName);
            }

            glGenTextures(1, &TextureID);
            glBindTexture(GL_TEXTURE_2D, TextureID);

            // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex_width, tex_height, 0, rgb_mode, GL_UNSIGNED_BYTE, tex_data);

            stbi_image_free(tex_data);

            // -------------------------------------------------------
            // 4. Ustawianie parametrow tekstury
            // -------------------------------------------------------

            // (a) Zachowanie wyjsca poza wspolrzedne UV (0-1) Wraping

            // GL_REPEAT, GL_MIRRORED_REPEAT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_BORDER
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

            // Ustalenie koloru ramki w przypadku wybrania GL_CLAMP_TO_BORDER
            //float color[] = { 1.0f, 0.0f, 0.0f, 1.0f };
            //glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);

            // (b) Zachowanie tekstury w przypadku powiekszenia

            // GL_LINEAR, GL_NEAREST
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            // (c) Zachowanie tekstury w przypadku pomniejszenia

            // GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_NEAREST,
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);


            // -------------------------------------------------------
            // 5. Gdy wybrano korzystanie z MIPMAP
            // -------------------------------------------------------

            // Generowanie mipmap automatycznie
            glGenerateMipmap(GL_TEXTURE_2D);

            // Podstawowy level mipmap (default 0)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
        }
        
        // Obliczenie wysokosci nad ziemia
        int UpdateLatitute()
        {
            if(Ground == NULL)
            {
                printf("ERROR Grond not set!!!");
            }
            float newAlt = Ground->getAltitute(glm::vec2(Position.x, Position.z), Position.y);

            if (newAlt != -100000.0f)\
            {
                Position.y = newAlt;
                _matObject = glm::translate(glm::mat4(1.0), Position);
                return 1;
            } else {
                return 0;
            }
        }

        // ustawienie pozycji na scenie
        void SetPosition(float x, float y, float z)
        {
            Position = glm::vec3(x, y, z);
            matModel = glm::translate(glm::mat4(1.0), Position);
        }
        // ustawienie pozycji na scenie
        void SetPosition(glm::vec3 pos)
        {
            Position = pos;
            matModel = glm::translate(glm::mat4(1.0), Position);
        }

        // zmiana pozycji na scenie
        void MoveXZ(float _x, float _z)
        {
            Position += glm::vec3(_x, 0.0, _z);

            // Aktualizacja polozenia na Y
            if(!UpdateLatitute())
            {
                Position -= glm::vec3(_x, 0.0, _z);
            }

        }

};


class Object_instanced : public Object
{

    public:
        const int instances = 500;
        // glm::mat4x4 * matModel;
        glm::mat4x4 matModel[500];
        
        int RES = 1000;
        int SIZE = 100;

        void setInstances(int inst)
        {
            // this->instances = inst;
            // matModel = new glm::mat4x4[inst];
            for (int i=0; i<instances; ++i)
            {
                matModel[i] = glm::mat4x4( 1.0f );
                float x = (rand()%RES)/(RES/SIZE) - SIZE/2.0f;
                float z = (rand()%RES)/(RES/SIZE) - SIZE/2.0f;
                float y = 0.0f;
                if (Ground != NULL)
                {
                    y = Ground->getAltitute(glm::vec2(x, z), 5.1f, 50.0f);
                }
                // y+= 0.5f;
                // if (y == -100000.0f)
                // {
                    // y = -2.0f;
                // }
                printf("kwiatek x: %f, y: %f, z: %f\n", x, y, z);
                float scale = (0.2f + (rand()%80)/100.0f);

                float angle = (rand()%314)/100.0f;
                matModel[i] = glm::translate(matModel[i], glm::vec3(x, y, z) );
                matModel[i] = glm::rotate(matModel[i], angle, glm::vec3(0.0f, 1.0f, 0.0f));
                matModel[i] = glm::scale(matModel[i], glm::vec3(scale, scale, scale));

            }

            // Vertex arrays
            glBindVertexArray( this->vArray );

            GLuint vInstances;
            glGenBuffers(1, &vInstances);
            glBindBuffer(GL_ARRAY_BUFFER, vInstances);
            glBufferData(GL_ARRAY_BUFFER, instances * sizeof(glm::mat4), &matModel[0], GL_STATIC_DRAW);

            glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);
            glEnableVertexAttribArray(3);
            glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));
            glEnableVertexAttribArray(4);
            glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
            glEnableVertexAttribArray(5);
            glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));
            glEnableVertexAttribArray(6);

            glVertexAttribDivisor(3, 1);
            glVertexAttribDivisor(4, 1);
            glVertexAttribDivisor(5, 1);
            glVertexAttribDivisor(6, 1);

            glBindVertexArray( 0 );
        }

        void draw()
        {

            
            for (int i=0; i<instances; ++i)
            {
                float a= 0.001f;
                float angle = a;
                if(frame > fps / 2.0f)
                {
                    angle = a;
                } else {
                    angle = -a;
                }
                matModel[i] = glm::rotate(matModel[i], angle, glm::vec3(0.0f, 1.0f, 0.0f));
            }


            glBindVertexArray( this->vArray );

            //Wspolrzedne Instance
            GLuint vInstances;
            glGenBuffers(1, &vInstances);
            glBindBuffer(GL_ARRAY_BUFFER, vInstances);
            glBufferData(GL_ARRAY_BUFFER, instances * sizeof(glm::mat4), &matModel[0], GL_STATIC_DRAW);

            glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);
            glEnableVertexAttribArray(3);
            glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));
            glEnableVertexAttribArray(4);
            glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
            glEnableVertexAttribArray(5);
            glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));
            glEnableVertexAttribArray(6);

            glVertexAttribDivisor(3, 1);
            glVertexAttribDivisor(4, 1);
            glVertexAttribDivisor(5, 1);
            glVertexAttribDivisor(6, 1);
            glBindVertexArray( 0 );

            glBindVertexArray( vArray );
            glDrawArraysInstanced(GL_TRIANGLES, 0, Size, this->instances); 
            glBindVertexArray( 0 );
        }
};

class Skybox
{
    public:
        GLuint SkyBox_VAO;
        GLuint SkyBox_Program;
        GLuint SkyBox_Texture;

        void CreateSkyBox()
        {
            GLfloat positions[8*3] =
            {
                1.0f, 1.0f, 1.0f,
                -1.0f, 1.0f, 1.0f,
                -1.0f, -1.0f, 1.0f,
                1.0f, -1.0f, 1.0f,
                1.0f, 1.0f, -1.0f,
                -1.0f, 1.0f, -1.0f,
                -1.0f, -1.0f, -1.0f,
                1.0f, -1.0f, -1.0f
            };

            GLuint indices[12*3] =
            {
                5, 0, 1,
                5, 4, 0,
                2, 0, 3,
                2, 1, 0,
                7, 0, 4,
                7, 3, 0,
                3, 6, 2,
                3, 7, 6,
                1, 2, 6,
                1, 6, 5,
                4, 5, 6,
                4, 6, 7
            };

            const char files[6][30] =
            {
                    "skybox/posx.jpg",
                    "skybox/negx.jpg",
                    "skybox/posy.jpg",
                    "skybox/negy.jpg",
                    "skybox/posz.jpg",
                    "skybox/negz.jpg",

            };

            const GLenum targets[6] =
            {
                    GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                    GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
                    GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                    GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
            };

            // Vertex arrays
            glGenVertexArrays( 1, &SkyBox_VAO );
            glBindVertexArray( SkyBox_VAO );

            // Wspolrzedne wierzchokow
            GLuint vBuffer_pos;
            glGenBuffers( 1, &vBuffer_pos );
            glBindBuffer( GL_ARRAY_BUFFER, vBuffer_pos );
            glBufferData( GL_ARRAY_BUFFER, 8*3*sizeof(GLfloat), positions, GL_STATIC_DRAW );
            glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
            glEnableVertexAttribArray( 0 );

            // Tablica indeksow
            GLuint vBuffer_idx;
            glGenBuffers( 1, &vBuffer_idx );
            glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vBuffer_idx );
            glBufferData( GL_ELEMENT_ARRAY_BUFFER, 12*3*sizeof( GLuint ), indices, GL_STATIC_DRAW );

            glBindVertexArray( 0 );

            // Tekstura CUBE_MAP
            glGenTextures( 1, &SkyBox_Texture);
            glBindTexture( GL_TEXTURE_CUBE_MAP, SkyBox_Texture );

            // Utworzenie 6 tekstur dla kazdej sciany
            for (int i=0; i < 6; ++i)
            {
                int tex_width, tex_height, n;
                unsigned char *tex_data;

                tex_data = stbi_load(files[i], &tex_width, &tex_height, &n, 0);
                if (tex_data == NULL)
                {
                    printf("Image %s can't be loaded!\n", files[i]);
                    exit(1);
                }
                // Zaladowanie danych do tekstury OpenGL
                glTexImage2D( targets[i], 0, GL_RGB, tex_width, tex_height, 0, GL_RGB, GL_UNSIGNED_BYTE, tex_data);

                // Zwolnienie pamieci pliku graficznego
                stbi_image_free(tex_data);

            }

            // Przykladowe opcje tekstury
            glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
            glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR );
            glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT );
            glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT );
            glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_REPEAT );
            glGenerateMipmap( GL_TEXTURE_CUBE_MAP );
            glBindTexture( GL_TEXTURE_CUBE_MAP, 0 );
            
        }
};
