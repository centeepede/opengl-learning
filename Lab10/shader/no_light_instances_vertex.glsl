#version 330

// Wspolrzedne wierzcholkow
layout( location = 0 ) in vec4 inPosition;
layout( location = 1 ) in vec3 inNormal;
layout( location = 2 ) in vec2 inUV;
layout( location = 3 ) in mat4 matModelInst;

// Macierz rzutowania
uniform mat4 matProjView;


out vec3 inoutNormal;
out vec2 inoutUV;


void main()
{
    inoutNormal = inNormal;
    inoutUV = inUV;

    // vec4 newPos = inPosition;
    // newPos.x += 5.0f * sin(gl_InstanceID)*(sqrt(gl_InstanceID/10.0f)) + 10.0f *rand(vec2(gl_InstanceID, 100.0f));
    // newPos.z += 5.0f * cos(gl_InstanceID)*(sqrt(gl_InstanceID/10.0f)) + 10.0f * rand(vec2(gl_InstanceID, 100.0f));

	// gl_Position = matProjView * newPos;
    gl_Position = matProjView  * matModelInst * inPosition;
}
