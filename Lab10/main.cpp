#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "animation.hpp"
#include "definitions.hpp"
#include "shader_stuff.hpp"
#include "texture_loader.hpp"
#include "obj_loader.hpp"
#include "object.hpp"
#include "light.hpp"
#include "menu.hpp"
#include "cfont.hpp"


int window_height;
int window_widht;

glm::mat4 matProj;
glm::mat4 matView;
glm::mat4 matMove;
glm::mat4 matProjView;


glm::vec3 Camera_Position;

glm::vec3 background_color;

Light key_light(-10.0f, 5.1f, 0.0f);
GLuint prog_no_light;
GLuint prog_no_light_instances;
GLuint prog_phong;
GLuint prog_gouraud;
GLuint prog_distance;
GLuint prog_fog;
GLuint prog_reflect;
GLuint prog_skybox;

Object objects[OBJECT_COUNT];
Object_instanced flowers;
Object light_test_box;
Object susan;
Skybox skybox;

TextMessage hud;
TextMessage fps_hud;

#include "ground.h"
#include "object.h"
CGround ziemia;
CSceneObject postac;

// ---------------------------------------
void DisplayScene()
{

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // Macierz widoku
    matView = glm::mat4x4( 1.0 );
    matView = glm::rotate( matView, _scene_rotate_x, glm::vec3( 1.0f, 0.0f, 0.0f ) );
    matView = glm::rotate( matView, _scene_rotate_y, glm::vec3( 0.0f, 1.0f, 0.0f ) );


    //------------- Skybox ------------------------

    glUseProgram(skybox.SkyBox_Program);

        // Przeskalowanie boxa i przeslanie macierzy rzutowania
        matProjView = matProj * matView * glm::scale(glm::mat4(1), glm::vec3(40.0, 40.0, 40.0));
        glUniformMatrix4fv( glGetUniformLocation( skybox.SkyBox_Program, "Matrix_proj_view" ), 1, GL_FALSE, glm::value_ptr(matProjView) );
        glUniform1i(glGetUniformLocation(skybox.SkyBox_Program, "tex0"), 0);

        // Aktywacja tekstury CUBE_MAP
        glActiveTexture(GL_TEXTURE0);
        glBindTexture( GL_TEXTURE_CUBE_MAP, skybox.SkyBox_Texture );

        // Rendering boxa
        glBindVertexArray( skybox.SkyBox_VAO );
            glDrawElements( GL_TRIANGLES, 12 * 3, GL_UNSIGNED_INT, NULL );
        glBindVertexArray( skybox.SkyBox_VAO );



    //Move view
    matMove = glm::translate( matMove, glm::vec3( _scene_translate_x, _scene_translate_y, _scene_translate_z ) );
    matView = matView * matMove;

    //CAMERA_POS for LIGHT purpuses
	Camera_Position = ExtractCameraPos(matView);

    // --------------- Ludzie i podłoga --------------------
    for (int i = 0; i < OBJECT_COUNT; i++)
    {
        if(i!=0)
        {
            objects[i].rotate(0.0f, 0.0f, 0.01f);
        }


        // Obliczanie macierzy rzutowania
        matProjView = matProj * matView * objects[i].getView();


            //LIGHT mode switch
            switch (light_mode)
            {
                case NO_LIGHT:
                    objects[i].idProgram = prog_no_light;
                    break;
                case PHONG:
                    objects[i].idProgram = prog_phong;
                    break;
                case GOURAUD:
                    objects[i].idProgram = prog_gouraud;
                    break;
                case LIGHT_DISTANCE:
                    objects[i].idProgram = prog_distance;
                    break;
                case LIGHT_FOG:
                    objects[i].idProgram = prog_fog;
                    break;
            }

        // Wlaczenie VAO i programu
        glUseProgram( objects[i].idProgram );

		// Przekazanie macierzy rzutowania
		glUniformMatrix4fv( glGetUniformLocation(objects[i].idProgram, "matProjView"), 1, GL_FALSE,  glm::value_ptr(matProjView));

        glUniform3fv(glGetUniformLocation(objects[i].idProgram, "background_color"),1,  &background_color[0] );


        //LIGHT
        key_light.sendUniform(objects[i].idProgram, Camera_Position);


		// TEXTURE
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, objects[i].TextureID);
		glUniform1i(glGetUniformLocation(objects[i].idProgram, "tex0"), 0);

        objects[i].draw();
    }

    //------------- Kwiatki ------------------------

    // Obliczanie macierzy rzutowania
    matProjView = matProj * matView * flowers.getView();

    // Wlaczenie VAO i programu
    glUseProgram( flowers.idProgram );

    // Przekazanie macierzy rzutowania
    glUniformMatrix4fv( glGetUniformLocation(flowers.idProgram, "matProjView"), 1, GL_FALSE,  glm::value_ptr(matProjView));
    glUniform3fv(glGetUniformLocation(flowers.idProgram, "background_color"),1,  &background_color[0] );
    //LIGHT Uniform
    key_light.sendUniform(flowers.idProgram, Camera_Position);


    // TEXTURE
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, flowers.TextureID);
    glUniform1i(glGetUniformLocation(flowers.idProgram, "tex0"), 0);



    flowers.draw();


    //------------- Susan ------------------------
        susan.MoveXZ(_postac_move_x, _postac_move_z);

        // Obliczanie macierzy rzutowania
        matProjView = matProj * matView * susan.getView() * glm::rotate( glm::mat4x4( 1.0f ), -_scene_rotate_y+3.0f, glm::vec3( 0.0f, 1.0f, 0.0f ) );

        // Wlaczenie VAO i programu
        glUseProgram( susan.idProgram );

        // Przekazanie macierzy rzutowania
        glUniformMatrix4fv( glGetUniformLocation(susan.idProgram, "matProjView"), 1, GL_FALSE,  glm::value_ptr(matProjView));
        glUniform3fv(glGetUniformLocation(susan.idProgram, "background_color"),1,  &background_color[0] );

        //LIGHT Uniform
        key_light.sendUniform(susan.idProgram, Camera_Position);


        // TEXTURE
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, susan.TextureID);
        glUniform1i(glGetUniformLocation(susan.idProgram, "tex0"), 0);

        //REFLECTION/REFRACTION
            // Aktywacja tekstury CUBE_MAP
        glActiveTexture(GL_TEXTURE1);
        glBindTexture( GL_TEXTURE_CUBE_MAP, skybox.SkyBox_Texture );
        glUniform1i(glGetUniformLocation(susan.idProgram, "tex1"), 1);

        susan.draw();


	// Wylaczanie
	glUseProgram( 0 );


    hud.drawHUD();
    fps_hud.drawFPS(fps);


	glutSwapBuffers();

}



// ---------------------------------------
void Initialize()
{

    matMove = glm::mat4x4( 1.0 );
	// _scene_translate_z = -10.0f;
	_scene_translate_y = -2.0f;

	background_color = glm::vec3( 0.2f, 0.2f, 0.2f);
	glClearColor(background_color.r, background_color.g, background_color.b, 1.0f);


    //1. Program bez światła
    prog_no_light = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_no_light, LoadShader(GL_VERTEX_SHADER, "shader/vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_no_light, LoadShader(GL_FRAGMENT_SHADER, "shader/fragment.glsl"));

    LinkAndValidateProgram( prog_no_light );

    //2. Program światło Phong
    prog_phong = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_phong, LoadShader(GL_VERTEX_SHADER, "shader/phong_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_phong, LoadShader(GL_FRAGMENT_SHADER, "shader/phong_fragment.glsl"));

    LinkAndValidateProgram( prog_phong );

    //3. Program światło Gouraud
    prog_gouraud = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_gouraud, LoadShader(GL_VERTEX_SHADER, "shader/gouraud_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_gouraud, LoadShader(GL_FRAGMENT_SHADER, "shader/gouraud_fragment.glsl"));

    LinkAndValidateProgram( prog_gouraud );

    //4. Program światło gouraud zanikające z odległością
    prog_distance = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_distance, LoadShader(GL_VERTEX_SHADER, "shader/distance_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_distance, LoadShader(GL_FRAGMENT_SHADER, "shader/distance_fragment.glsl"));

    LinkAndValidateProgram( prog_distance );

    //5. Mgła
    prog_fog = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_fog, LoadShader(GL_VERTEX_SHADER, "shader/fog_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_fog, LoadShader(GL_FRAGMENT_SHADER, "shader/fog_fragment.glsl"));

    LinkAndValidateProgram( prog_fog );

    //6. Reflective
    printf("reflective program program INIT...\n");
    prog_reflect = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_reflect, LoadShader(GL_VERTEX_SHADER, "shader/reflect_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_reflect, LoadShader(GL_FRAGMENT_SHADER, "shader/reflect_fragment.glsl"));

    LinkAndValidateProgram( prog_reflect );

    //7. Skybox
    printf("skybox program INIT...\n");
    prog_skybox = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_skybox, LoadShader(GL_VERTEX_SHADER, "shader/skybox_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_skybox, LoadShader(GL_FRAGMENT_SHADER, "shader/skybox_fragment.glsl"));

    LinkAndValidateProgram( prog_skybox );

    //8. No light (plane with alpha texture) instances
    printf("no light program INIT...\n");
    prog_no_light_instances = glCreateProgram();

    printf("loading vertex shader...\n");
    glAttachShader( prog_no_light_instances, LoadShader(GL_VERTEX_SHADER, "shader/no_light_instances_vertex.glsl"));
    printf("loading fragment shader...\n");
    glAttachShader( prog_no_light_instances, LoadShader(GL_FRAGMENT_SHADER, "shader/no_light_instances_fragment.glsl"));

    LinkAndValidateProgram( prog_no_light_instances );

    //---------------------------------------------

    printf("FINISHED programs INIT...\n");

	glEnable(GL_DEPTH_TEST);
    glEnable( GL_BLEND );
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //---------- Ludzie i podłoga -----------------
    printf("ludzie INIT...\n");
    for (int i = 0; i < OBJECT_COUNT; i++)
    {
        // printf("ludzie light mode...\n");
        switch (light_mode)
        {
            case NO_LIGHT:
                objects[i].idProgram = prog_no_light;
                break;
            case PHONG:
                objects[i].idProgram = prog_phong;
                break;
            case GOURAUD:
                objects[i].idProgram = prog_gouraud;
                break;
            case LIGHT_DISTANCE:
                objects[i].idProgram = prog_distance;
                break;
            case LIGHT_FOG:
                objects[i].idProgram = prog_distance;
                break;
        }


        // printf("ludzie init...\n");
        switch (i)
        {
            case 0:
                objects[i].init("obj/base.obj", "tex/base.bmp");
                break;
            case 1:
                objects[i].init("obj/humans.obj", "tex/humans.bmp");
                break;
            case 2:
                objects[i].init("obj/bride_clothes.obj", "tex/bride_clothes.bmp");
                break;
            case 3:
                objects[i].init("obj/groom_clothes.obj", "tex/groom_clothes.bmp");
                break;
        }


        // printf("FINISHED ludzie init...\n");
    }

    //---------- Ground -----------------
    printf("ground INIT...\n");
    //CGround ziemia;
    ziemia.CreateFromOBJ(objects[0].OBJ_vertices);

    //---------- Kwiatki -----------------
    printf("kwiatki INIT...\n");
    flowers.idProgram = prog_no_light_instances;
    flowers.init("obj/flower.obj", "tex/flower.png", &ziemia);
    flowers.setInstances(500);
    // flowers.translate(4.0f, 0.0f, 0.0f);

    //---------- Susan -----------------
    printf("susan INIT...\n");
    susan.idProgram = prog_reflect;
    susan.init("obj/susan.obj", "tex/susan.png", &ziemia);
    susan.translate(0.0f, 0.5f, 0.0f);
    // susan.rotate(0.0f, 5.0f);

    //---------- Skybox -----------------
    printf("skybox INIT...\n");
    skybox.SkyBox_Program = prog_skybox;
    skybox.CreateSkyBox();



    //----------- Fonts ----------------

    hud.init("font/xirod.ttf");
    fps_hud.init("font/space_age.ttf");


}

// ---------------------------------------
void Reshape( int width, int height )
{
	glViewport( 0, 0, width, height );

	matProj = glm::ortho(-1.0, 1.0, -1.0, 1.0);
	matProj = glm::perspective(glm::radians(80.0f), width/(float)height, 0.1f, 200.0f);

    hud.reshape(width, height);
    fps_hud.reshape(width, height);

    window_widht = width;
    window_height = height;

}


// ---------------------------------------------------
int main( int argc, char *argv[] )
{
	// GLUT
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH );
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );
	glutInitWindowSize( 500, 500 );
	glutCreateWindow( "Programownie grafiki w OpenGL" );

	// -----------------------------
	// NOWE: Handlery
	// -----------------------------

	glutDisplayFunc( DisplayScene );
	glutReshapeFunc( Reshape );
	glutMouseFunc( MouseButton );
	glutMotionFunc( MouseMotion );
	glutMouseWheelFunc( MouseWheel );
	glutKeyboardFunc( KeyDown );
	glutKeyboardUpFunc( KeyUp );
	glutSpecialFunc( SpecialKeys );


	// GLEW
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if( GLEW_OK != err )
	{
		printf("GLEW Error\n");
		exit(1);
	}

	// OpenGL
	if( !GLEW_VERSION_3_2 )
	{
		printf("Brak OpenGL 3.2!\n");
		exit(1);
	}

    //Menu

    int subMenuLight = glutCreateMenu( menu_stuff );
    glutAddMenuEntry( "Ambient", MENU_NO_LIGHT );
    glutAddMenuEntry( "Phong", MENU_PHONG );
    glutAddMenuEntry( "Gouraud", MENU_GOURAUD );
    glutAddMenuEntry( "Ambient", MENU_NO_LIGHT );
    glutAddMenuEntry( "Distance", MENU_LIGHT_DISTANCE );
    glutAddMenuEntry( "Fog", MENU_LIGHT_FOG );

    // Kolejne podmenu
    int SubMenuDummy = glutCreateMenu( menu_stuff );
    glutAddMenuEntry( "Dummy 1", -1 );
    glutAddMenuEntry( "Dummy 2", -1 );

    // Na koniec utworzenie glownego menu
    glutCreateMenu( menu_stuff );
    glutAddSubMenu( "Swiatlo ogolne", subMenuLight );
    glutAddSubMenu( "Dummy", SubMenuDummy );

    glutAttachMenu( GLUT_RIGHT_BUTTON );


	Initialize();

       // glutTimerFunc (100 , Animation , 0);
	glutTimerFunc ((int)(1000/60) , Animation ,0);
	glutMainLoop (); 



	// Cleaning
	glDeleteProgram(prog_no_light);
	glDeleteProgram(prog_phong);
	glDeleteProgram(prog_gouraud);
	glDeleteProgram(prog_distance);
	glDeleteProgram(prog_fog);
	glDeleteProgram(prog_skybox);
	glDeleteProgram(prog_reflect);
	glDeleteProgram(prog_no_light_instances);

	return 0;
}
