#include <stdio.h>
#include "definitions.hpp"

#define MENU_NO_LIGHT 0
#define MENU_PHONG 1
#define MENU_GOURAUD 2
#define MENU_LIGHT_DISTANCE 3
#define MENU_LIGHT_FOG 4

int light_mode = PHONG;

void menu_stuff (int value)
{
    switch( value )
        {
        case MENU_PHONG:
            printf("Wybrano opcja nr jeden\n")	;
            light_mode = PHONG;
            break;
            
        case MENU_GOURAUD:
            printf("Wybrano opcje nr dwa\n")	;
            light_mode = GOURAUD;
            break;
            
        case MENU_NO_LIGHT:
            printf("Wybrano opcje nr dwa\n")	;
            light_mode = NO_LIGHT;
            break;
        
        case MENU_LIGHT_FOG:
            printf("Wybrano opcje nr dwa\n")	;
            light_mode = LIGHT_FOG;
            break;
        
        case MENU_LIGHT_DISTANCE:
            printf("Wybrano opcje nr dwa\n")	;
            light_mode = LIGHT_DISTANCE;
            break;
        
        default:
            printf("Wybrano %d \n", value);
        }
}
