# Projekt

* Poruszanie się klawiszami WSAD oraz góra/dół Spacja/V
* Klawisze 1-5 zmieniają oświetlenie

## Lab 11:

***znajduje sie w folderze projektV0.1***

* Zadanie 1 **Implemented** 
    + Kamienie - nie pozwalają na ruch
    + Kwiatki - można przez nie przejść
    + Monety - można zebrać
    ![screenshot](za1-1.png)
* Zadanie 2 ***not implemented***
* Zadanie 3 ***not implemented***
