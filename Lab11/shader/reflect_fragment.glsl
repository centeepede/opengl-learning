#version 420 core

in vec3 ourPosition;
in vec3 inoutNormal;
in vec3 ourColor;
in vec2 inoutUV;
in vec3 camPos;

// uniform vec3 color_base;
layout(binding=0) uniform sampler2D tex0;
layout(binding=1) uniform samplerCube tex1;

out vec4 outColor;

void main()
{

	// ustawienie domyslnego koloru fragmentu
	// mozna tutaj uwzglednic tekstury i inne parametry
	// vec4 objectColor = vec4(color_base, 1.0f);
    // vec4 objectColor = vec4(0.2f, 0.8f, 0.2f, 1.0f);
    vec4 objectColor = texture( tex0, inoutUV ) ;// + vec4(inoutColor, 1.0) ;

    vec3 Dir = normalize(ourPosition - camPos);
    vec3 Reflection = reflect(Dir, inoutNormal);
    vec4 ReflectionColor = vec4(texture(tex1, Reflection).rgb, 1.0f);

    float Refraction_Coeff = 1.0f/1.2f;
    vec3 Refraction = refract(Dir, inoutNormal, Refraction_Coeff);
    vec4 RefractionColor = vec4(texture(tex1, Refraction).rgb, 1.0f);

    // outColor = mix((vec4(ourColor, 1.0f) * objectColor), ReflectionColor, 0.5f);
    outColor = mix(mix((vec4(ourColor, 1.0f) * objectColor), ReflectionColor, 0.5f), RefractionColor, 0.5f);

}
