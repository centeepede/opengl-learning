#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_stuff.hpp"
class Program{

    public:
        GLuint idProgram;

        Program(const char * fragment, const char * vertex)
        {

            idProgram = glCreateProgram();

            printf("loading vertex shader...");
            glAttachShader( idProgram, LoadShader(GL_VERTEX_SHADER, vertex));
            printf("loading fragment shader...");
            glAttachShader( idProgram, LoadShader(GL_FRAGMENT_SHADER, fragment));

            LinkAndValidateProgram( idProgram );
        }

};
