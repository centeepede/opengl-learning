#version 150

in vec4 inoutPos;
in vec3 inoutNormal;
in vec2 inoutUV;
out vec4 outColor;

uniform vec3 color_base;
uniform sampler2D tex0;
uniform float xoffset;


void main()
{
    // float l = inoutNormal.r+inoutNormal.g+inoutNormal.b;

    // vec3 myColor = vec3(l, l, l) * color_base;

    // outColor = vec4(myColor, 1.0);
    outColor = texture( tex0, vec2(inoutUV.x+xoffset, inoutUV.y )) ;// + vec4(inoutColor, 1.0) ;
}
