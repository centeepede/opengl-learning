#include <stdio.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


#include "shader_stuff.hpp"
#include "obj_loader.hpp"
#include "object.hpp"

#include "texture_loader.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define OBJECT_COUNT 1


glm::mat4 matProj;
glm::mat4 matView;
glm::mat4 matProjView;

float xoffset;

GLuint TextureID;

// Identyfikatory obiektow

Object objects[OBJECT_COUNT];


// ---------------------------------------
void DisplayScene()
{

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // Macierz widoku
    matView = glm::mat4x4( 1.0 );
    matView = glm::translate( matView, glm::vec3( _scene_translate_x, _scene_translate_y, _scene_translate_z+_mouse_zoom ) );
    matView = glm::rotate( matView, _scene_rotate_x, glm::vec3( 1.0f, 0.0f, 0.0f ) );
    matView = glm::rotate( matView, _scene_rotate_y, glm::vec3( 0.0f, 1.0f, 0.0f ) );
    // --------------- I --------------------
    for (int i = 0; i < OBJECT_COUNT; i++)
    {


        // Obliczanie macierzy rzutowania
        matProjView = matProj * objects[i].getView(matView);


        // Wlaczenie VAO i programu
        glBindVertexArray( objects[i].idVAO );
        glUseProgram( objects[i].idProgram );

		// Przekazanie macierzy rzutowania
		glUniformMatrix4fv( glGetUniformLocation(objects[i].idProgram, "matProjView"), 1, GL_FALSE,  glm::value_ptr(matProjView));

	glUniform3f(glGetUniformLocation(objects[i].idProgram, "color_base"), objects[i].r(), objects[i].g(), objects[i].b() );

		// TEXTURE
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, objects[i].TextureID);
		glUniform1i(glGetUniformLocation(objects[i].idProgram, "tex0"), 0);

        xoffset = _tex_offset;
		glUniform1f(glGetUniformLocation(objects[i].idProgram, "xoffset"), xoffset);


		// Generowanie obiektow na ekranie
		glDrawArrays( GL_TRIANGLES, 0, objects[i].OBJ_vertices().size() );
        

    }
	// Wylaczanie
	glUseProgram( 0 );
	glBindVertexArray( 0 );


	glutSwapBuffers();

}

//------------------------------------
void CreateTexture(Object &object, const char * texFName)
{
	int tex_width;
	int tex_height;
    int comp;
	// unsigned char *tex_data;
    unsigned char* tex_data = stbi_load(texFName, &tex_width, &tex_height, &comp, STBI_rgb_alpha);

	// loadBMP_custom(texFName, tex_width, tex_height, &tex_data);

	glGenTextures(1, &object.TextureID);
	glBindTexture(GL_TEXTURE_2D, object.TextureID);

    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    if (comp == 3)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tex_width, tex_height, 0, GL_RGB, GL_UNSIGNED_BYTE, tex_data);
        printf("success1");
    } 
    else if (comp == 4)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex_width, tex_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex_data);
        printf("success2");
    } 


    stbi_image_free(tex_data);

	// -------------------------------------------------------
	// 4. Ustawianie parametrow tekstury
	// -------------------------------------------------------

	// (a) Zachowanie wyjsca poza wspolrzedne UV (0-1) Wraping

	// GL_REPEAT, GL_MIRRORED_REPEAT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_BORDER
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Ustalenie koloru ramki w przypadku wybrania GL_CLAMP_TO_BORDER
	//float color[] = { 1.0f, 0.0f, 0.0f, 1.0f };
	//glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);

	// (b) Zachowanie tekstury w przypadku powiekszenia

	// GL_LINEAR, GL_NEAREST
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// (c) Zachowanie tekstury w przypadku pomniejszenia

	// GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_NEAREST,
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);


	// -------------------------------------------------------
	// 5. Gdy wybrano korzystanie z MIPMAP
	// -------------------------------------------------------

	// Generowanie mipmap automatycznie
	glGenerateMipmap(GL_TEXTURE_2D);

	// Podstawowy level mipmap (default 0)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);

}



// ---------------------------------------
void Initialize()
{
	_scene_translate_z = -10.0f;

	glClearColor( 0.2f, 0.2f, 0.2f, 1.0f );

	glEnable(GL_DEPTH_TEST);

    for (int i = 0; i < OBJECT_COUNT; i++)
    {
        switch (i)
        {
            case 0:
                CreateTexture(objects[i], "background.png");
                objects[i].init("square.obj", 0.5f, 0.5f, 0.5f);
                break;
        }

        // 1. Program i shadery
        objects[i].idProgram = glCreateProgram();

        glAttachShader( objects[i].idProgram, LoadShader(GL_VERTEX_SHADER, "vertex.glsl"));
        glAttachShader( objects[i].idProgram, LoadShader(GL_FRAGMENT_SHADER, "fragment.glsl"));

        LinkAndValidateProgram( objects[i].idProgram );


        // 2. Vertex arrays
        glGenVertexArrays( 1, &objects[i].idVAO );
        glBindVertexArray( objects[i].idVAO );

        // Bufor na wspolrzedne wierzcholkow
        glGenBuffers( 1, &objects[i].idVBO_coord );
        glBindBuffer( GL_ARRAY_BUFFER, objects[i].idVBO_coord );
        glBufferData( GL_ARRAY_BUFFER, objects[i].OBJ_vertices().size() * sizeof(glm::vec3), &objects[i].OBJ_vertices()[0], GL_STATIC_DRAW );
        glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
        glEnableVertexAttribArray( 0 );

        // Bufor na wspolrzedne wierzcholkow
        glGenBuffers( 1, &objects[i].idVBO_normal );
        glBindBuffer( GL_ARRAY_BUFFER, objects[i].idVBO_normal );
        glBufferData( GL_ARRAY_BUFFER, objects[i].OBJ_normals().size() * sizeof(glm::vec3), &objects[i].OBJ_normals()[0], GL_STATIC_DRAW );
        glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, NULL );
        glEnableVertexAttribArray( 1 );

        // Wspolrzedne textury UV
        glGenBuffers( 1, &objects[i].idVBO_uv );
        glBindBuffer( GL_ARRAY_BUFFER, objects[i].idVBO_uv );
        glBufferData( GL_ARRAY_BUFFER, objects[i].OBJ_uvs().size() * sizeof(glm::vec3), &objects[i].OBJ_uvs()[0], GL_STATIC_DRAW );
        glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, 0, NULL );
        glEnableVertexAttribArray( 2 );
    }

    glBindVertexArray( 0 );


}

// ---------------------------------------
void Reshape( int width, int height )
{
	glViewport( 0, 0, width, height );

	matProj = glm::ortho(-1.0, 1.0, -1.0, 1.0);
	matProj = glm::perspective(glm::radians(80.0f), width/(float)height, 0.1f, 20.0f);

}




// ---------------------------------------------------
int main( int argc, char *argv[] )
{
	// GLUT
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );
	glutInitWindowSize( 500, 500 );
	glutCreateWindow( "Programownie grafiki w OpenGL" );

	// -----------------------------
	// NOWE: Handlery
	// -----------------------------

	glutDisplayFunc( DisplayScene );
	glutReshapeFunc( Reshape );
	glutMouseFunc( MouseButton );
	glutMotionFunc( MouseMotion );
	glutMouseWheelFunc( MouseWheel );
	glutKeyboardFunc( Keyboard );
	glutSpecialFunc( SpecialKeys );


	// GLEW
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if( GLEW_OK != err )
	{
		printf("GLEW Error\n");
		exit(1);
	}

	// OpenGL
	if( !GLEW_VERSION_3_2 )
	{
		printf("Brak OpenGL 3.2!\n");
		exit(1);
	}


	Initialize();


	glutMainLoop();


	// Cleaning
    for (int i = 0; i < OBJECT_COUNT; i++)
    {
	glDeleteProgram( objects[i].idProgram );
	glDeleteVertexArrays( 1, &objects[i].idVBO_coord );
	glDeleteVertexArrays( 1, &objects[i].idVAO );
	glDeleteVertexArrays( 1, &objects[i].idVBO_uv );

    }

	return 0;
}
