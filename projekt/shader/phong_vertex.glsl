#version 330 core

layout( location = 0 ) in vec4 inPosition;
layout( location = 1 ) in vec3 inNormal;
layout( location = 2 ) in vec2 inUV;

out vec3 ourPosition;
out vec3 myPosition;
out vec3 inoutNormal;
out vec3 myNormal;
out vec3 inoutColor;
out vec2 inoutUV;

uniform mat4 matProjView;
uniform mat4 matModel;

void main()
{
    myNormal = transpose(inverse(mat3(matModel))) * inNormal;
    inoutNormal = inNormal;
    inoutUV = inUV;
	gl_Position = matProjView * inPosition;

	// W Modelu Phonga, oswietlenie obiektu obliczane
	// jest na poziomie fragmentu, a nie wierzcholka
	// jak w modelu Gourauda. Wysylamy wiec wszystkie
	// dane wierzcholka jak pozycje i wektor normalny
	// do shadera fragmentow

	ourPosition = vec3(inPosition);
	myPosition = vec3(matModel * inPosition);

}
