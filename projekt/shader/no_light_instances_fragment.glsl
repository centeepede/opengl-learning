#version 330

in vec3 inoutNormal;
in vec3 myPosition;
in vec2 inoutUV;
out vec4 outColor;

uniform vec3 color_base;
uniform sampler2D tex0;

uniform vec3 Camera_Position;

void main()
{
    // float l = inoutNormal.r+inoutNormal.g+inoutNormal.b;

    // vec3 myColor = vec3(l, l, l) * color_base;

    vec4 texColor = texture( tex0, inoutUV ) ;// + vec4(inoutColor, 1.0) ;


    vec3 camFrag = Camera_Position - myPosition;
    float fogDistance = length(camFrag);

    float a = 1000.0f;
    float b = 0.46f;

    float fogAmount = a * exp(-fogDistance*b);
    // float fogAmount = a * exp(-Camera_Position.z*b) * ( 1.0f-exp( -fogDistance*camFrag.z*b ) ) / (b*camFrag.z);
    if(fogAmount < 0.0f)
    {
        fogAmount = 0.0f;
    } else if(fogAmount > 1.0f)
    {
        fogAmount = 1.0f;
    }
    // fogAmount = 1.0f - fogAmount;


	// Zastosowanie oswietlenia do fragmentu
    texColor.a = fogAmount * texColor.a;

    if(texColor.a < 0.1f)
    {
        discard;
    }
    // outColor = vec4(myColor, 1.0);
    outColor = texColor;
}
