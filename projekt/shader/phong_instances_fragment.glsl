#version 150 core

in vec3 ourPosition;
in vec3 myPosition;
in vec3 inoutNormal;
in vec3 myNormal;
in vec2 inoutUV;

out vec4 outColor;

uniform sampler2D tex0;
// Parametry oswietlenia
uniform vec3 Light_Ambient;
uniform vec3 Light_Diffuse;
uniform vec3 Light_Specular;
uniform vec3 Light_Position;
uniform vec3 Camera_Position;

// ---------------------------------------------------------------------------
vec3 Calculate_PointLight(vec3 fragPos, vec3 fragNormal)
{
	// ---------------------------
	// Ambient
	vec3 ambientPart = Light_Ambient;


	// ---------------------------
	// Diffuse

	// Obliczenie wektora (swiatlo - wierzcholek)
	// czyli kierunku padania swiatla na wierzcholek
	vec3 lightDirection = normalize(Light_Position - vec3(fragPos));

	// obliczenie kata pomiedzy wektorem lightDir oraz wektorem normalnym
	// wartosc kata okresla pod jakim katem padaja promienie
	float lightCoeff = max(dot(fragNormal, lightDirection), 0.0);

	vec3 diffusePart = lightCoeff * Light_Diffuse;


	// ------------------
	// Specular
	vec3 viewDir = normalize(Camera_Position - vec3(fragPos));
	vec3  reflectDir = reflect(-lightDirection, fragNormal);
	// obliczanie wspolczynnika specular z parametrem shininess
	float specularCoeff = pow(max(dot(viewDir, reflectDir), 0.0), 128.0f);
	vec3  specularPart = specularCoeff * Light_Specular;

	// -----------------
	// Ostateczny
	return (ambientPart + diffusePart + specularPart);
}

float cap (float a, float ma, float mi)
{
    if (a > ma)
    {
        a = ma;
    }
    if (a < mi)
    {
        a = mi;
    }
    return a;
}

void main()
{

	// ustawienie domyslnego koloru fragmentu
	// mozna tutaj uwzglednic tekstury i inne parametry
	// vec3 objectColor = vec3(0.2, 0.8, 0.2);
    vec4 objectColor = texture( tex0, inoutUV ) ;// + vec4(inoutColor, 1.0) ;

    vec3 camFrag = Camera_Position - myPosition;
    float fogDistance = length(camFrag);

    float a = 1000.0f;
    float b = 0.46f;

    // float fogAmount = a * exp(-Camera_Position.z*b) * ( 1.0f-exp( -fogDistance*camFrag.z*b ) ) / (b*camFrag.z);
    float fogAmount = a * exp(-fogDistance*b);
    if(fogAmount < 0.0f)
    {
        fogAmount = 0.0f;
    } else if(fogAmount > 1.0f)
    {
        fogAmount = 1.0f;
    }
    // fogAmount = 1.0f - fogAmount;


	// Zastosowanie oswietlenia do fragmentu
    vec4 ourColor = vec4(Calculate_PointLight(myPosition, myNormal), fogAmount);
 
    if(ourColor.a < 0.1f)
    {
        discard;
    }
    ourColor.r = cap(ourColor.r, 1.2f, 0.0f);
    ourColor.g = cap(ourColor.g, 1.2f, 0.0f);
    ourColor.b = cap(ourColor.b, 1.2f, 0.0f);

	// outColor = mix(vec4(Calculate_PointLight(ourPosition, inoutNormal), 1.0f) * objectColor, vec4(background_color, 1.0f), fogAmount);
    outColor = ourColor * objectColor;
}
