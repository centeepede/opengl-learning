#version 330 core

layout( location = 0 ) in vec4 inPosition;
layout( location = 1 ) in vec3 inNormal;
layout( location = 2 ) in vec2 inUV;
layout( location = 3 ) in mat4 matModelInst;

out vec3 inoutNormal;
out vec3 ourColor;
out vec2 inoutUV;

uniform mat4 matProjView;
uniform mat4 matModel;

// Parametry oswietlenia
uniform vec3 Light_Ambient;
uniform vec3 Light_Diffuse;
uniform vec3 Light_Specular;
uniform vec3 Light_Position;
uniform vec3 Camera_Position;


// ---------------------------------------------------------------------------
vec3 Calculate_PointLight(vec3 fragPos, vec3 fragNormal)
{
	// ---------------------------
	// Ambient
	vec3 ambientPart = Light_Ambient;


	// ---------------------------
	// Diffuse

	// Obliczenie wektora (swiatlo - wierzcholek)
	// czyli kierunku padania swiatla na wierzcholek
	vec3 lightDirection = normalize(Light_Position - vec3(fragPos));

	// obliczenie kata pomiedzy wektorem lightDir oraz wektorem normalnym
	// wartosc kata okresla pod jakim katem padaja promienie
	float lightCoeff = max(dot(fragNormal, lightDirection), 0.0f);

	vec3 diffusePart = lightCoeff * Light_Diffuse;


	// ------------------
	// Specular
	vec3 viewDir = normalize(Camera_Position - vec3(fragPos));
	vec3  reflectDir = reflect(-lightDirection, fragNormal);
	// obliczanie wspolczynnika specular z parametrem shininess
	float specularCoeff = pow(max(dot(viewDir, reflectDir), 0.0), 128.0f);
	vec3  specularPart = specularCoeff * Light_Specular;

	// -----------------
	// Ostateczny
	return (ambientPart + diffusePart + specularPart);
}

float cap (float a, float ma, float mi)
{
    if (a > ma)
    {
        a = ma;
    }
    if (a < mi)
    {
        a = mi;
    }
    return a;
}

void main()
{
    vec3 myNormal = transpose(inverse(mat3(matModelInst))) * inNormal ;
    inoutNormal = inNormal;
    inoutUV = inUV;
    // gl_Position = matProjView * inPosition;
    gl_Position = matProjView  * matModelInst * inPosition;

	// przeslanie obliczonej wartosci swiatla
	// do fragment shadera, ktory go uwzgledni
    ourColor = Calculate_PointLight(vec3(matModelInst * inPosition), myNormal );
    // ourColor = Calculate_PointLight(vec3(inPosition), inNormal );
    // ourColor.r = cap(ourColor.r, 1,0f, 0.0f);
    // ourColor.g = cap(ourColor.g, 1,0f, 0.0f);
    // ourColor.b = cap(ourColor.b, 1,0f, 0.0f);
}
