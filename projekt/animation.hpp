#include <ctime>
#include "shader_stuff.hpp"
// #include "definitions.hpp"

bool won = false;
float _postac_move_z = 0.0f;
float _postac_move_x = 0.0f;
bool print_pos = false;
int frame = 0;
int fps = 60;
time_t previous_time = time(NULL);
//---------------------------------------
void Animation(int keyframe)
{

    if (keys[27])
    {
        exit(0);
    }

    frame ++;
    if (previous_time + 1 <= time(NULL))
    {
        previous_time = time(NULL);
        fps = frame;
        frame = 0;
    }

    //movement
    const float MOVE = 0.15f;

    //scene movement
    float dir_mod_z = 0.0f;
    float dir_mod_x = 0.0f;

    //postac movement
    float postac_mod_z = 0.0f;
    float postac_mod_x = 0.0f;

    if (!won)
    {
        if ( keys ['w'] )
        {
            // dir_mod_z += MOVE*cos(-_scene_rotate_y);
            // dir_mod_x += MOVE*sin(-_scene_rotate_y);
            postac_mod_z += -MOVE*cos(-_scene_rotate_y);
            postac_mod_x += -MOVE*sin(-_scene_rotate_y);
        }
        if ( keys ['s'] )
        {
            // dir_mod_z += -MOVE*cos(-_scene_rotate_y);
            // dir_mod_x += -MOVE*sin(-_scene_rotate_y);
            postac_mod_z += MOVE*cos(-_scene_rotate_y);
            postac_mod_x += MOVE*sin(-_scene_rotate_y);
        }
        if ( keys ['a'] )
        {
            // dir_mod_z += MOVE*sin(_scene_rotate_y);
            // dir_mod_x += MOVE*cos(_scene_rotate_y);
            postac_mod_z += -MOVE*sin(_scene_rotate_y);
            postac_mod_x += -MOVE*cos(_scene_rotate_y);
        }
        if ( keys ['d'] )
        {
            // dir_mod_z += -MOVE*sin(_scene_rotate_y);
            // dir_mod_x += -MOVE*cos(_scene_rotate_y);
            postac_mod_z += MOVE*sin(_scene_rotate_y);
            postac_mod_x += MOVE*cos(_scene_rotate_y);
        }


        if ( keys ['i'] )
        {
            postac_mod_z += -MOVE*cos(-_scene_rotate_y);
            postac_mod_x += -MOVE*sin(-_scene_rotate_y);
        }
        if ( keys ['k'] )
        {
            postac_mod_z += MOVE*cos(-_scene_rotate_y);
            postac_mod_x += MOVE*sin(-_scene_rotate_y);
        }
        if ( keys ['j'] )
        {
            postac_mod_z += -MOVE*sin(_scene_rotate_y);
            postac_mod_x += -MOVE*cos(_scene_rotate_y);
        }
        if ( keys ['l'] )
        {
            postac_mod_z += MOVE*sin(_scene_rotate_y);
            postac_mod_x += MOVE*cos(_scene_rotate_y);
        }


        if ( keys ['v'] )
        {
            _scene_translate_y = MOVE;
        }else if ( keys [' '] )
        {
            _scene_translate_y = -MOVE;
        }else{
            _scene_translate_y = 0.0f;
        }

        if ( keys ['p'] )
        {
            print_pos = true;
        }else{
            print_pos = false;
        }

        // //LIGHT change
        // if ( keys ['1'] )
        // {
            // light_mode = NO_LIGHT;
        // }
        // if ( keys ['2'] )
        // {
            // light_mode = PHONG;
        // }
        // if ( keys ['3'] )
        // {
            // light_mode = GOURAUD;
        // }
        // if ( keys ['4'] )
        // {
            // light_mode = LIGHT_DISTANCE;
        // }
        // if ( keys ['5'] )
        // {
            // light_mode = LIGHT_FOG;
        // }

    }


        _scene_translate_z = dir_mod_z;
        _scene_translate_x = dir_mod_x;
        _postac_move_z = postac_mod_z;
        _postac_move_x = postac_mod_x;
        glutPostRedisplay ();
        glutTimerFunc ((int)(1000/60) , Animation ,0);
}


